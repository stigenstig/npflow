import pytest
import numpy as np
from scipy.stats import norm

from npflow.constants import k_b, temp, mu0, d_p
from npflow.particles import Particles
from npflow.scaling import Scale


@pytest.mark.parametrize("nparticles", [500, 1000, 2000])
@pytest.mark.parametrize("iterations", [10, 500])
def test_particles_compare_fit_to_analytical(nparticles, iterations, default_tube_network, default_tube_flow):
    network = default_tube_network
    q = default_tube_flow
    u = q[0]/network.link_crossections[0]
    s = Scale()
    nps = Particles(network, nparticles, s)
    D = k_b * temp / (3 * np.pi * mu0 * d_p)
    D_undim = s.undim_length(s.undim_velocity(D))
    dx_max = 0.1 * network.link_length
    dt = np.min([np.min(dx_max * network.link_crossections[:] / (np.abs(q[:]))), dx_max ** 2 / (2 * D_undim)]) / 100

    for i in range(iterations):
        nps.move_particles(q, dt, D_undim)

    t = i*dt

    data = nps.particles['pos'].copy()

    l_factor = int(u * t)
    x = np.linspace(min(data), max(data), 100)
    mean_data, std_data = norm.fit(data)
    C = 1 / np.sqrt(4 * np.pi * D_undim * t) * np.exp(-(x - u * t + l_factor) ** 2 / (4 * D_undim * t))
    mean_exact = x[np.where(C == np.max(C))]
    std_exact = np.sqrt(2*D_undim*t)

    # TODO: The difference in mean sometimes is relatively large
    #assert np.abs(std_data-std_exact) < 1e-1 and np.abs(mean_data-mean_exact) < 1e-1
    #assert np.abs(std_data - std_exact) < 1e-1
    assert np.abs(mean_data - mean_exact) < 1e-1

