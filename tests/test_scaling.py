import pytest
import math
import numpy as np

# math.isclose has default relative tolerance of 1e-9
from npflow.constants import k_b


def test_scaling_undim_length(scale, base_parameters):
    param_undim = scale.undim_length(base_parameters["l0"])
    assert math.isclose(1, param_undim)


def test_scaling_undim_viscosity(scale, base_parameters):
    param_undim = scale.undim_viscosity(base_parameters["mu0"])
    assert math.isclose(1, param_undim)


def test_scaling_undim_density(scale, base_parameters):
    param_undim = scale.undim_density(base_parameters["rho0"])
    assert math.isclose(1, param_undim)


def test_scaling_undim_dim_length(scale, base_parameters):
    param = base_parameters["l0"]
    assert math.isclose(param, scale.dim_length(scale.undim_length(param)))


def test_scaling_undim_dim_viscosity(scale, base_parameters):
    param = base_parameters["mu0"]
    assert math.isclose(param, scale.dim_viscosity(scale.undim_viscosity(param)))


def test_scaling_undim_dim_density(scale, base_parameters):
    param = base_parameters["rho0"]
    assert math.isclose(param, scale.dim_density(scale.undim_density(param)))


def test_scaling_undim_dim_velocity(scale, base_parameters):
    param = 1
    assert math.isclose(param, scale.dim_velocity(scale.undim_velocity(param)))


def test_scaling_undim_dim_timestep(scale, base_parameters):
    param = 1
    assert math.isclose(param, scale.dim_timestep(scale.undim_timestep(param)))


def test_scaling_undim_dim_pressure(scale, base_parameters):
    param = 1
    assert math.isclose(param, scale.dim_pressure(scale.undim_pressure(param)))


def test_scaling_undim_dim_flow_rate(scale, base_parameters):
    param = 1
    assert math.isclose(param, scale.dim_flow_rate(scale.undim_flow_rate(param)))


def test_scaling_undim_dim_reynolds(scale, base_parameters, default_tube_network, default_tube_flow):
    D = k_b * base_parameters["T"] / (3 * np.pi * base_parameters["mu0"] * base_parameters["d_p"])
    D_undim = scale.undim_velocity(scale.undim_length(D))
    q_undim = default_tube_flow[0]
    a_undim = default_tube_network.link_crossections[0]
    L_undim = 1
    rho_undim = 1
    mu_undim = 1

    re_undim = (L_undim*q_undim*rho_undim)/(a_undim*mu_undim)

    re_dim = (scale.dim_length(L_undim)*scale.dim_flow_rate(q_undim)*
              scale.dim_density(rho_undim))/(scale.dim_length(scale.dim_length(a_undim))*
                                               scale.dim_viscosity(mu_undim))
    assert math.isclose(re_undim, re_dim)


def test_scaling_undim_dim_peclet(scale, base_parameters, default_tube_network, default_tube_flow):
    D = k_b * base_parameters["T"] / (3 * np.pi * base_parameters["mu0"] * base_parameters["d_p"])
    D_undim = scale.undim_velocity(scale.undim_length(D))
    q_undim = default_tube_flow[0]
    a_undim = default_tube_network.link_crossections[0]
    L_undim = 1
    rho_undim = 1
    mu_undim = 1

    pe_undim = (L_undim*q_undim)/(a_undim*D_undim)

    pe_dim = (scale.dim_length(L_undim)*scale.dim_flow_rate(q_undim))/\
             (scale.dim_length(scale.dim_length(a_undim))*D)

    assert math.isclose(pe_undim, pe_dim)

