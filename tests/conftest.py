import pytest

from npflow.constants import d_c, l0, d_l, d_p, temp, mu0, rho0
from npflow.networks import OpenHexagonalNetwork, SimpleOpenTubeNetwork
from npflow.solve_pressure import solve_node_pressures
from npflow.flows import calculate_link_flows
from npflow.scaling import Scale

import numpy as np
np.random.seed(123)

@pytest.fixture
def base_parameters():
    base_params = {
        "N":        200,
        "d_c":      d_c,
        "l0":       l0,
        "d_l":      d_l,
        "d_p":      d_p,
        "T":        temp,
        "mu0":      mu0,
        "rho0":     rho0,
        "P0":       10,
        "P1":       0,
        "lx":       14,
        "ly":       20,
        "dP_per_l": 2.5e5
    }
    return base_params

@pytest.fixture
def scale():
    return Scale()


@pytest.fixture
def default_hex_network():
    """Returns a default OpenHexagonalnetwork instance"""
    network = OpenHexagonalNetwork()
    network.setup()
    return network


@pytest.fixture
def default_tube_network():
    network = SimpleOpenTubeNetwork()
    network.setup()
    return network


@pytest.fixture
def default_tube_flow(default_tube_network, scale):
    p0, p1 = 10, 0
    p = solve_node_pressures(default_tube_network, p0, p1)
    q = calculate_link_flows(default_tube_network, p, scale.undim_viscosity(scale.mu0))
    return q


@pytest.fixture
def default_hex_flow(default_hex_network, scale):
    p0, p1 = 10, 0
    p = solve_node_pressures(default_hex_network, p0, p1)
    q = calculate_link_flows(default_hex_network, p, scale.undim_viscosity(scale.mu0))
    return q


@pytest.fixture
def random_hex_network():
    network = OpenHexagonalNetwork(random_volume=True)
    network.setup()
    return network


@pytest.fixture
def random_hex_flow(random_hex_network, scale):
    p0, p1 = 10, 0
    p = solve_node_pressures(random_hex_network, p0, p1)
    q = calculate_link_flows(random_hex_network, p, scale.undim_viscosity(scale.mu0))
    return q

