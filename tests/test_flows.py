import numpy as np


def test_default_hex_network_inlet_outlet_flow(default_hex_network, default_hex_flow):
    flow_in = default_hex_flow[:default_hex_network.nx // 2]
    flow_out = default_hex_flow[default_hex_network.nlinks - default_hex_network.nx - 1::default_hex_network.nneighbours]
    assert np.abs(np.sum(flow_in)-np.sum(flow_out)) < 1e-6


def test_random_hex_network_inlet_outlet_flow(random_hex_network, random_hex_flow):
    flow_in = random_hex_flow[:random_hex_network.nx // 2]
    flow_out = random_hex_flow[random_hex_network.nlinks - random_hex_network.nx - 1::random_hex_network.nneighbours]
    assert np.abs(np.sum(flow_in)-np.sum(flow_out)) < 1e-6
