import pytest
import numpy as np

from npflow.networks import OpenHexagonalNetwork
from npflow.utils.exceptions import SizeNotEvenException, SmallSizeException


def test_default_hex_network_size(default_hex_network):
    assert default_hex_network.nx == 6 and default_hex_network.ny == 4


@pytest.mark.parametrize("nx,ny,total", [
    (6, 6, 42),
    (8, 4, 40),
    (8, 8, 72)
])
def test_network_total_nodes(nx, ny, total):
    network = OpenHexagonalNetwork(nx, ny)
    assert network.nnodes == total


def test_network_odd_nx_raises_exception():
    with pytest.raises(SizeNotEvenException):
        network = OpenHexagonalNetwork(nx=5)


def test_network_odd_ny_raises_exception():
    with pytest.raises(SizeNotEvenException):
        network = OpenHexagonalNetwork(ny=5)


def test_network_small_nx_raises_exception():
    with pytest.raises(SmallSizeException):
        network = OpenHexagonalNetwork(nx=4)


def test_network_small_ny_raises_exception():
    with pytest.raises(SmallSizeException):
        network = OpenHexagonalNetwork(ny=2)


@pytest.mark.parametrize("node,node_neighbours", [
    (4, [3, 5, 9]),
    (10, [9, 11, 17]),
    (15, [14, 16, 20]),
    (28, [24, 0, 0]),
])
def test_default_hex_network_node_neighbours(node, node_neighbours, default_hex_network):
    assert np.array_equal(default_hex_network.node_node_neighbours[node, :].sort(), node_neighbours.sort())


@pytest.mark.parametrize("node,link_neighbours", [
    (6, [6, 7, 8]),
    (14, [18, 19, 20]),
    (28, [35, 0, 0])
])
def test_default_hex_network_link_neightbors(node, link_neighbours, default_hex_network):
    assert np.array_equal(default_hex_network.node_link_neighbours[node, :].sort(), link_neighbours.sort())
