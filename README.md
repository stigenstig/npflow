# ⬡ npflow


A tool based on network simulations used to understand the effect of ultrasound on nanoparticles and drugs in the interstitium.

## Features
To be implemented.

## Install as a package

1. Create a virtualenv.
2. Run `python setup.py install` to install the package

## Tests
To be implemented.

