import numpy as np
from npflow.networks import SimpleOpenTubeNetwork, SimpleOpenDoubleTubeNetwork, OpenHexagonalNetwork, Network
from npflow.custom_types import NodeTypes
from npflow.flows import calculate_link_mobilities
from npflow.scaling import Scale


def solve_node_pressures(network, p0, p1):
    """

    :type network: Network
    """
    A = np.zeros((network.nnodes, network.nnodes))
    b = np.zeros(network.nnodes)

    mu = 1
    g_links = calculate_link_mobilities(network, mu)

    if isinstance(network, OpenHexagonalNetwork):
        b[0:network.nx // 2] = p0
        b[network.nnodes - network.nx // 2:] = p1
    elif isinstance(network, SimpleOpenTubeNetwork):
        b[0] = p0
        b[-1] = p1

    for node in network.nodes:
        # inlet or outlet row
        if network.node_type[node] == NodeTypes.NODE_INLET.value \
                or network.node_type[node] == NodeTypes.NODE_OUTLET.value:
            A[node][node] = 1
        else:
            # node is of normal type
            for neighbour in range(network.nneighbours):
                neighbour_index = network.node_node_neighbours[node, neighbour]
                link = network.node_link_neighbours[node, neighbour]
                g = g_links[link]
                A[node, neighbour_index] = -g
                A[node, node] += g

    p = np.linalg.solve(A, b)
    return p


def solve_elastic_node_pressures(network, scale, p_n, params, dt):
    """

    :type network: Network
    :type scale: Scale
    """
    A = np.zeros((network.nnodes, network.nnodes))
    b = np.zeros(network.nnodes)

    L = scale.undim_length(params["l0"])
    mu = scale.undim_viscosity(params["mu0"])
    E = scale.undim_pressure(params["E"])
    d = scale.undim_length(params["d"])

    g_link = np.pi * network.link_radii ** 4 / (8 * mu * L)
    h_link = np.pi * network.link_radii * network.link_length\
         * network.link_radius ** 2 / (E * d)
    if isinstance(network, SimpleOpenTubeNetwork):
        b[0] = p_n[0]
        b[-1] = p_n[-1]

    for node in network.nodes:
        # inlet or outlet row
        if network.node_type[node] == NodeTypes.NODE_INLET.value \
                or network.node_type[node] == NodeTypes.NODE_OUTLET.value:
            A[node][node] = 1

        else:
            # links in back of node, i.e. link 8 for node 11
            node_back_links = np.where(network.link_node_front == node)
            # links in front of node, i.e. link 13, 15 for node 11
            node_front_links = np.where(network.link_node_back == node)

            for link in network.node_link_neighbours[node]:
                # if node is in front of link, get node at back of link
                if node == network.link_node_front[link]:
                    neigh_node = network.link_node_back[link]
                # if node is in back of link, get node at front of link
                elif node == network.link_node_back[link]:
                    neigh_node = network.link_node_front[link]

                A[node, node] += +g_link[link] + +h_link[link] / (2*dt)
                A[node, neigh_node] = -g_link[link] + h_link[link] / (2*dt)
                b[node] += h_link[link] / (2*dt) * (p_n[neigh_node] + p_n[node])

    p = np.linalg.solve(A, b)
    return p


def solve_simple_double_tubenetwork(network):
    """

    :type network: SimpleOpenDoubleTubeNetwork
    """
    A = np.zeros((network.nnodes, network.nnodes))

    P0 = 10

    A[0][0] = 1

    A[1][0] = g
    A[1][1] = -3 * g
    A[1][2] = g
    A[1][3] = g

    A[2][1] = g
    A[2][2] = -2 * g
    A[2][4] = g

    A[3][1] = g
    A[3][3] = -2 * g
    A[3][4] = g

    A[4][2] = g
    A[4][3] = g
    A[4][4] = -3 * g
    A[4][5] = g

    A[5][5] = 1

    print(A)

    b = np.zeros(network.nnodes)
    b[0] = P0
    b[-1] = P0 / 2

    p = np.linalg.solve(A, b)

    return p
