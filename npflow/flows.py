import numpy as np


def calculate_link_mobilities(network, mu):
    g = np.zeros(network.nlinks)

    for link in range(network.nlinks):
        g[link] = np.pi * network.link_radii[link] ** 4 / (8 * network.link_length*mu)

    return g


def calculate_link_flows(network, p, mu):

    links_flow = np.zeros(network.nlinks)
    g_links = calculate_link_mobilities(network, mu=mu)

    for link in range(network.nlinks):
        node_front = network.link_node_front[link]
        node_back = network.link_node_back[link]
        link_g = g_links[link]
        links_flow[link] = -link_g*(p[node_front]-p[node_back])

    return links_flow
