import multiprocessing
import matplotlib as mpl
import time

from joblib import Parallel, delayed
from scipy.stats import norm

from npflow.basecase import BaseCase

import numpy as np


np.random.seed(123)

mpl.rcParams.update(mpl_params)


def mean_particle_distance(params, i):
    base_case = BaseCase(params=params)

    start_node = base_case.ohn.nnodes // 2 - 4*base_case.ohn.nx + 1
    end_node = start_node + base_case.ohn.nx

    start_link = np.where(base_case.ohn.link_node_back == start_node)[0]
    end_link = np.where(base_case.ohn.link_node_back == end_node)[0]
    index_diff = 0

    for particle in range(base_case.nps.nparticles):
        new_link = start_link + index_diff % (end_link - start_link)
        base_case.nps.particles[particle]['link'] = new_link
        base_case.nps.particles[particle]['pos'] = params["x0"]
        index_diff += 3

    nps_start_y = base_case.nps.cartesian_coordinates[:, 1].copy()

    t = 0.0
    t_max = base_case.params["t_max"]
    t_max_undim = base_case.scaling.undim_timestep(t_max)
    dt = base_case.dt
    D_undim = base_case.D_undim
    q = base_case.flow
    u = np.min(q/base_case.ohn.link_crossections)

    print(f"t_sim={base_case.scaling.dim_timestep(D_undim/(u**2))} seconds")

    while t < t_max_undim:
        if t + dt > t_max_undim:
            dt = t_max_undim-t
        base_case.nps.move_particles(q, dt, D_undim)
        t += dt

    print(f"{np.sum(base_case.nps.particles['link'] == -1)} particles outside network")

    q_tot = np.sum(base_case.flow[:base_case.ohn.nx//2])

    # The Peclet number
    pe = base_case.peclet_number
    # The Reynolds number
    re = base_case.reynolds_number

    displacement = base_case.nps.cartesian_coordinates[:, 1]
    displacement = base_case.scaling.dim_length(displacement-nps_start_y)
    mean, std = norm.fit(displacement)
    print(f"mean={mean}")

    return mean, std, pe, re, q_tot


def calc_mean_particle_distance(i, params, key, param, x):
    params[key] = param * x[i]
    print(f"{key} is now {params[key]}")

    return mean_particle_distance(params, i)


def run_job(params, key, npoints, x):
    base_param = params[key]
    num_cores = multiprocessing.cpu_count()
    print(f"The number of cores is {num_cores}")
    time1 = time.time()
    mean, std, pe, re, q_tot = zip(*Parallel(n_jobs=num_cores - 1, verbose=100)(
            delayed(calc_mean_particle_distance)(i, params, key, base_param, x) for i in range(npoints)))
    time2 = time.time()

    print(f"Calculations took {(time2-time1):.2f} seconds")

    return mean, std, pe, re, q_tot

