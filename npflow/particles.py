import numpy as np
from npflow.networks import Network
from npflow.constants import iy, d_p
from npflow.scaling import Scale
from npflow.custom_types import NodeTypes, LinkTypes


class Particles(object):

    def __init__(self, network, nparticles, scale):
        """

        :type network: Network
        :type scale: Scale
        """
        d = np.dtype([('link', 'i8'), ('pos', 'f'), ('radius', 'f')])

        self.network = network
        self.nparticles = nparticles
        self.particles = np.zeros(nparticles, dtype=d)
        self.np_cart_coord = np.zeros((nparticles, 2))
        self.scale = scale
        self.neighbour_q_out = np.zeros(self.network.nneighbours)

        self.allocate_particles()

    def allocate_particles(self):
        particle = 0
        y_threshold = 1e-12
        running = True
        inlet_links = np.array(np.where(self.network.link_type == LinkTypes.LINK_INLET.value))[0]
        while running:
            for link in inlet_links:
                node = self.network.link_node_back[link]
                if self.network.node_positions[node, iy] < y_threshold:
                    self.particles[particle]['link'] = link
                    self.particles[particle]['pos'] = 0.0
                    # print(f'particle {particle} set in link {link}')
                    particle += 1
                    if particle == self.nparticles:
                        running = False
                        break

    def move_particles(self, links_flow, dt, D):
        particle = 0

        while particle < self.nparticles:

            link = self.particles[particle]['link']

            if link < 0 or self.scale.undim_length(d_p) > self.network.link_radii[link]*2.0:
                particle += 1
                continue

            # advective_part
            dx_adv = links_flow[link] / self.network.link_crossections[link] * dt
            r = np.random.standard_normal()
            dx_diff = r * np.sqrt(2 * D * dt)

            self.calculate_advection_step(particle, links_flow, dx_adv)
            self.calculate_diffusion_step(particle, dx_diff)

            particle += 1

    @property
    def cartesian_coordinates(self):

        for p in range(self.nparticles):
            link = self.particles[p]['link']
            x0 = self.network.link_positions[link, 0]
            y0 = self.network.link_positions[link, 1]
            x1 = self.network.link_positions[link, 2]
            y1 = self.network.link_positions[link, 3]

            p_pos = self.particles[p]['pos']

            if np.abs(x0 - x1) < 1e-12:
                theta = np.pi / 2
            else:
                theta = np.arctan((y1 - y0) / (x1 - x0))

            if theta < 0:
                theta += np.pi

            xp = x0 + p_pos * np.cos(theta)
            yp = y0 + p_pos * np.sin(theta)

            self.np_cart_coord[p, :] = [xp, yp]

        return self.np_cart_coord

    def calculate_advection_step(self, particle, links_flow, dx_new):
        new_link = False
        link = self.particles[particle]['link']
        self.particles[particle]['pos'] += dx_new
        # Handle movement into new link
        if self.particles[particle]['pos'] > self.network.link_length:
            # particle moves through front node
            new_link = True
            node = self.network.link_node_front[link]
            dt_new = np.abs(self.particles[particle]['pos'] - self.network.link_length) * \
                     self.network.link_crossections[link] / abs(links_flow[link])
            # self.particles[particle]['pos'] = self.network.link_length

        elif self.particles[particle]['pos'] < 0.0:
            # particle moves through back node
            new_link = True
            node = self.network.link_node_back[link]
            dt_new = abs(dx_new) * \
                     self.network.link_crossections[link] / abs(links_flow[link])

        if new_link:

            self.neighbour_q_out[:] = 0

            if self.network.node_type[node] == NodeTypes.NODE_NORMAL.value:
                # Add flow rates out of node
                for n in range(self.network.nneighbours):
                    # Iterate over neighbouring nodes
                    neigh_link = self.network.node_link_neighbours[node, n]
                    if (links_flow[neigh_link] > 0.0 and node == self.network.link_node_back[neigh_link]) or (
                            links_flow[neigh_link] < 0.0 and node == self.network.link_node_front[neigh_link]):
                        if self.network.link_radii[neigh_link] > self.particles[particle]["radius"]:
                            self.neighbour_q_out[n] = abs(links_flow[neigh_link])
                    else:
                        self.neighbour_q_out[n] = 0

                if np.sum(self.neighbour_q_out) > 0.0:
                    weights = self.neighbour_q_out / np.sum(self.neighbour_q_out)
                    next_link = np.random.choice(a=self.network.node_link_neighbours[node], size=1, p=weights)[0]

                    # calculate position in new link
                    self.particles[particle]['link'] = next_link

                    if node == self.network.link_node_front[next_link]:
                        # node is in front of new link

                        self.particles[particle]['pos'] = (self.network.link_length + \
                                                           links_flow[next_link] / self.network.link_crossections[
                                                               next_link] * dt_new) * 0

                    elif node == self.network.link_node_back[next_link]:
                        # node is in back of new link
                        self.particles[particle]['pos'] = (links_flow[next_link] / self.network.link_crossections[
                            next_link] * dt_new) * 0
                    # print(f"Adv: Particle {particle} moved from link {link} to {next_link}")
                    # print(f"Adv: Particle {particle} has position {self.particles[particle]['pos']:.3f}")

            elif self.network.node_type[node] == NodeTypes.NODE_INLET.value:
                # particle tries to exit through inlet
                self.particles[particle]['pos'] = 0
                self.particles[particle]['link'] = -1

            else:
                self.particles[particle]['link'] = -1
                self.particles[particle]['pos'] = 0.0

    def calculate_diffusion_step(self, particle, dx_diff):
        diff_pos_tmp = self.particles[particle]['pos'] + dx_diff
        link = self.particles[particle]['link']

        if diff_pos_tmp > self.network.link_length or diff_pos_tmp < 0:
            pos_tmp = 0
            new_pos = 0
            if diff_pos_tmp > self.network.link_length:
                # particle moved forward and reached a node in front of link
                node = self.network.link_node_front[link]
                pos_tmp = diff_pos_tmp - self.network.link_length
            elif diff_pos_tmp < 0:
                node = self.network.link_node_back[link]
                # particle moved backwards and reached a node in back of link
                pos_tmp = np.abs(diff_pos_tmp)

            if self.network.node_type[node] == NodeTypes.NODE_NORMAL.value:
                # find links connected to node that is not current link
                link_indices = np.where(self.network.node_link_neighbours[node] != link)
                possible_links = self.network.node_link_neighbours[node][link_indices]

                weights = self.network.link_crossections[possible_links] / \
                          np.sum(self.network.link_crossections[possible_links])
                # selected link based on crossection
                next_link = np.random.choice(a=possible_links, size=1, p=weights)[0]

                if self.network.link_node_back[next_link] == node:
                    # new link has node in back
                    new_pos = pos_tmp
                elif self.network.link_node_front[next_link] == node:
                    # new link has node in front
                    new_pos = self.network.link_length - pos_tmp

                self.particles[particle]['link'] = next_link
                self.particles[particle]['pos'] = new_pos

            elif self.network.node_type[node] == NodeTypes.NODE_INLET.value:
                self.particles[particle]['link'] = -1
                #print(f"Link of particle {particle} set to -1")

        else:
            self.particles[particle]['pos'] += dx_diff




