import numpy as np

from npflow.constants import d_c, l0, d_l, d_p, temp, k_b, rho0, mu0, r_l_ratio
from npflow.flows import calculate_link_flows
from npflow.networks import OpenHexagonalNetwork
from npflow.particles import Particles
from npflow.scaling import Scale
from npflow.solve_pressure import solve_node_pressures

base_params = {
    "N":        10000,
    "d_c":      d_c,
    "l0":       l0,
    "l_factor": 1,
    "d_l":      d_l,
    "d_p":      d_p,
    "T":        temp,
    "mu0":      mu0,
    "rho0":     rho0,
    "P0":       10,
    "P1":       0,
    "lx":       20, # in micro meter
    "ly":       60, # in micro meter
    "dP_per_l": 2.5e5,
    "t_max": 300,
    "D_eff": 1,
    "D": k_b*temp/(3 * np.pi *
                  mu0*d_p),
    "x0":     0.5,
    "dx_max": 1,
    "npoints": 20,
    "random_volume": False
}


class BaseCase(object):
    def __init__(self, params, **kwargs):
        self.scaling = Scale(l0=params["l0"])
        self.params = params
        if kwargs is not None:
            for key, value in kwargs.items():
                self.params[key] = value

        self.setup_network()
        self.setup_flow()
        self.setup_particles()

    def setup_network(self):
        nx = self.params["lx"] // np.cos(np.pi / 6.0)
        ny = self.params["ly"] // (1 + np.sin(np.pi / 6.0))
        nx = int(nx // 2) * 2
        ny = int(ny // 2) * 2
        self.ohn = OpenHexagonalNetwork(nx=nx, ny=ny, random_volume=self.params["random_volume"])
        self.ohn.link_radius = self.params["d_l"] / (2 * self.scaling.r0) * r_l_ratio
        self.ohn.link_length *= self.params["l_factor"]
        self.ohn.setup()

    def setup_flow(self):
        self.dP = self.params["dP_per_l"] * (self.ohn.ny + 1)*self.ohn.dy * self.params["l0"]*self.params["l_factor"]
        self.dp = self.scaling.undim_pressure(self.dP)

        self.pressure = solve_node_pressures(self.ohn, self.dp, self.params["P1"])
        self.flow = calculate_link_flows(self.ohn, self.pressure,
                                         self.scaling.undim_viscosity(self.params["mu0"]))
        #print(f"Max flow velocity is {np.max(self.scaling.dim_velocity(self.flow/self.ohn.link_crossections))}")

    def setup_particles(self):
        D_eff= k_b * self.params["T"] / (3 * np.pi *
                  self.params["mu0"] * self.params["d_p"])
        self.params["D"] = D_eff
        D_eff *= self.params["D_eff"]

        self.D_undim = self.scaling.undim_length(self.scaling.undim_velocity(D_eff))
        self.dx_max = self.params["dx_max"]
        alpha = 5
        self.dt = np.min([np.min(self.dx_max * self.ohn.link_crossections[:] /
                            (np.abs(self.flow[:]))), self.dx_max ** 2 / (2 * self.D_undim)]) / alpha
        self.nps = Particles(self.ohn, self.params["N"], self.scaling)
        self.nps.particles["pos"] = self.params["x0"]*self.ohn.link_length

    def move_particles(self, n_iters):
        for i in range(n_iters):
            self.nps.move_particles(self.flow, self.dt, self.D_undim)


    @property
    def peclet_number(self):
        pe = np.min(self.ohn.link_length * self.flow[:] /
                    (self.ohn.link_crossections[:] * self.D_undim))
        return pe

    @property
    def reynolds_number(self):
        re = np.max(self.ohn.link_length * self.flow[:] * self.params["rho0"] /
                    (self.ohn.link_crossections * self.params["mu0"]))
        return re
