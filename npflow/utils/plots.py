import os

import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm, linregress
from npflow.scaling import Scale
from npflow.utils.filesave import save_latex_code_to_file
from npflow.utils.latex_config import latex_keys, latex_param


def plot_if_short(x0, y0, x1, y1, ax, link_lw, linecolor):
    if (x0 - x1) ** 2 + (y0 - y1) ** 2 <= 1.1:
        ax.plot([x0, x1], [y0, y1], color=linecolor, linestyle='-', linewidth=link_lw, zorder=1)
        return True
    return False


def plot_histogram_and_norms(data, nbins, iterations, t, u, D, x0):
    s = Scale()
    data = s.dim_length(data*1e6)
    print(np.mean(data))
    plt.hist(data, bins=nbins, density=True)
    l_factor = int(u * t)
    x = np.linspace(min(data), max(data), 100)
    mean, std = norm.fit(data)
    a_mean = s.dim_length((u * t - l_factor+x0)*1e6)
    a_std = s.dim_length(np.sqrt(2*D*t)*1e6)
    C = 1 / np.sqrt(2*np.pi * a_std**2) * np.exp(-(x - a_mean) ** 2 / (2*a_std**2))
    fx = 1 / (std * np.sqrt(2 * np.pi)) * np.exp(-(x - mean) ** 2 / (2 * std ** 2))

    plt.plot(x, fx, label='fit to data')
    plt.plot(x, C, label='analytical')
    plt.xlabel("$x [\si{\micro\meter}]$")
    plt.ylabel("Probability density [\si{\micro\meter^{-1}}]")
    print(f"For {iterations} iterations (time={s.dim_timestep(t)*1e6:.4f})):")
    print(f"Analytical: mu={a_mean:.10f}, sigma={a_std:.12f}")
    print(f"rel err:  mu={(mean-a_mean)/(a_mean)*100:.3f}%, "
          f"sigma={(std-a_std)/a_std*100:.3f}%")


def plot_params_exponent(x, mean, std, x_exp, mu_exp, sigma_exp, t_max, param_key, save_dir):

    if param_key == "d_l":
        x *= 1e9
        x_exp *= 1e9
    elif param_key == "mu0":
        x *= 1e3
        x_exp *= 1e3
    elif param_key == "dP_per_l":
        x *= 1e-3
        x_exp *= 1e-3
    elif param_key == "D_eff":
        x *= 1e12
        x_exp *= 1e12


    plt.figure(0)
    plt.errorbar(x, mean * 1e6, yerr=std * 1e6, fmt='-o')
    plt.xlabel(f"${latex_param[param_key]} {latex_keys[param_key]}$")
    plt.ylabel("Particle penetration depth $[\si{\micro\meter}$]")
    plt.errorbar(x_exp, mu_exp*1e6, yerr=sigma_exp*1e6, fmt='-*')
    plt.savefig(save_dir / f'params_exponent_mean_t_{t_max}_{param_key}.png', bbox_inches='tight')
    plt.close()

    plt.figure(1)
    plt.plot(x, std * 1e6, '-o')
    plt.plot(x_exp, sigma_exp * 1e6, '*')
    plt.xlabel(f"${latex_param[param_key]} {latex_keys[param_key]}$")
    plt.ylabel("$\sigma_{x} [\si{\\micro\meter}]$")
    plt.savefig(save_dir / f'params_exponent_std_t_{t_max}_{param_key}.png', bbox_inches='tight')
    plt.close()


def plot_params(x, y, error, param_key, pe, re, q_tot, save_dir):
    s = Scale()

    if param_key == "d_l":
        x *= 1e9
    elif param_key == "mu0":
        x *= 1e3
    elif param_key == "dP_per_l":
        x *= 1e-3
    elif param_key == "D_eff":
        x *= 1e12

    q = s.dim_flow_rate(q_tot)

    plt.figure(0)
    plt.errorbar(x, y*1e6, yerr=error*1e6, fmt='-o')
    plt.xlabel(f"${latex_param[param_key]} {latex_keys[param_key]}$")
    plt.ylabel("Particle penetration depth $[\si{\micro\meter}$]")
    plt.savefig(save_dir / f'params_analysis_displacement_{param_key}.png', bbox_inches='tight')

    plt.figure(1)
    plt.plot(x, pe, '-o')
    plt.xlabel(f"${latex_param[param_key]} {latex_keys[param_key]}$")
    plt.ylabel("Peclet number")
    plt.savefig(save_dir / f'params_analysis_pe_{param_key}.png', bbox_inches='tight')

    plt.figure(2)
    plt.plot(x, re, '-o')
    plt.xlabel(f"${latex_param[param_key]} {latex_keys[param_key]}$")
    plt.ylabel("Reynolds number")
    plt.savefig(save_dir / f'params_analysis_re_{param_key}.png', bbox_inches='tight')

    # log log of parameter
    y_log = np.log10(y)
    x_log = np.log10(x)
    plt.figure(3)
    plt.plot(x_log, y_log, '-o')
    plt.xlabel(f"${latex_param['log10']}{latex_param[param_key]}$")
    plt.ylabel(f"${latex_param['log10']}$ Particle penetration depth")
    
    slope, intercept, r_value, p_value, std_err = linregress(x_log, y_log)
    plt.plot(x_log, x_log * slope + intercept, label=f'$\\beta$ = {slope:.3f}\n$R^{{2}}={r_value**2:.3f}$')
    plt.legend()
    plt.savefig(save_dir / f'params_analysis_displacement_loglog_{param_key}.png', bbox_inches='tight')

    # log log of peclet number
    pe_log = np.around(np.log10(pe), decimals=5)
    plt.figure(4)
    plt.plot(x_log, pe_log, '-o')
    plt.xlabel(f"${latex_param['log10']}{latex_param[param_key]}$")
    plt.ylabel(f"${latex_param['log10']}$ Peclet number")

    slope, intercept, r_value, p_value, std_err = linregress(x_log, pe_log)
    plt.plot(x_log, x_log * slope + intercept, label=f'$\\beta$ = {slope:.3f}\n$R^{{2}}={r_value**2:.3f}$')
    plt.legend()
    plt.savefig(save_dir / f'params_analysis_pe_loglog_{param_key}.png', bbox_inches='tight')

    # log log of reynolds number
    re_log = np.log10(re)
    plt.figure(5)
    plt.plot(x_log, re_log, '-o')
    plt.xlabel(f"${latex_param['log10']}{latex_param[param_key]}$")
    plt.ylabel(f"${latex_param['log10']}$ Reynolds number")
    
    slope, intercept, r_value, p_value, std_err = linregress(x_log, re_log)
    plt.plot(x_log, x_log * slope + intercept, label=f'$\\beta$ = {slope:.3f}\n$R^{{2}}={r_value**2:.3f}$')
    plt.legend()
    plt.savefig(save_dir / f'params_analysis_re_loglog_{param_key}.png', bbox_inches='tight')

    plt.figure(6)
    plt.plot(x, q, '-o')
    plt.xlabel(f"${latex_param[param_key]} {latex_keys[param_key]}$")
    plt.ylabel("Q $[\si{\meter\cubed\per\second}$]")
    plt.savefig(save_dir / f'params_analysis_q_{param_key}.png', bbox_inches='tight')

    plt.figure(7)
    q_log = np.log10(q)
    plt.plot(x_log, q_log, '-o')
    plt.xlabel(f"${latex_param['log10']}{latex_param[param_key]}$")
    plt.ylabel(f"${latex_param['log10']}$ Q")

    slope, intercept, r_value, p_value, std_err = linregress(x_log, q_log)
    plt.plot(x_log, x_log * slope + intercept, label=f'$\\beta$ = {slope:.3f}\n$R^{{2}}={r_value**2:.3f}$')
    plt.legend()
    plt.savefig(save_dir / f'params_analysis_q_loglog_{param_key}.png', bbox_inches='tight')

    # plot standard deviation and its log-log plot
    plt.figure(8)
    std = error
    plt.plot(x, std*1e6, '-o')
    plt.xlabel(f"${latex_param[param_key]} {latex_keys[param_key]}$")
    plt.ylabel("$\sigma_{x} [\si{\\micro\meter}]$")
    plt.savefig(save_dir / f'params_analysis_std_{param_key}.png', bbox_inches='tight')

    plt.figure(9)
    std_log = np.log10(std)
    plt.plot(x_log, std_log, '-o')
    plt.xlabel(f"${latex_param['log10']}{latex_param[param_key]}$")
    plt.ylabel(f"${latex_param['log10']} \sigma_{{x}}$ ")

    slope, intercept, r_value, p_value, std_err = linregress(x_log, std_log)
    plt.plot(x_log, x_log * slope + intercept, label=f'$\\beta$ = {slope:.3f}\n$R^{{2}}={r_value**2:.3f}$')
    plt.legend()
    plt.savefig(save_dir / f'params_analysis_std_loglog_{param_key}.png', bbox_inches='tight')

    for i in range(10):
        plt.clf()
        fig = plt.figure(i)
        plt.close(fig)

    save_latex_code_to_file(save_dir, param_key, latex_param[param_key], x, y*1e6)
