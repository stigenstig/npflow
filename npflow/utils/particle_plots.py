import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import hist

from npflow.networks import OpenHexagonalNetwork
from npflow.particles import Particles


def plot_particles(ax, np_cart_coords):
    """

    :type network: OpenHexagonalNetwork
    :type nps: Particles
    """
    for p in range(len(np_cart_coords[:, 0])):
        x = np_cart_coords[p, 0]
        y = np_cart_coords[p, 1]
        ax.plot(x, y, "ro", markersize=2)

    return ax



