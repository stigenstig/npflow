import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
from scipy.stats import norm

from npflow.constants import k_b, temp, mu0, d_p
from npflow.flows import calculate_link_flows
from npflow.networks import SimpleOpenTubeNetwork
from npflow.particles import Particles
from npflow.scaling import Scale
from npflow.solve_pressure import solve_node_pressures
from npflow.utils.plots import plot_histogram_and_norms

params = {'pgf.texsystem': 'pdflatex',
          'text.usetex': True,
          'font.family': 'serif',
          #'font.serif': 'Computer Modern Roman',
          #'figure.figsize':   (7, 7),
          #'font.size': 8,
          'axes.labelsize':   8,
          #'axes.titlesize':   26,
          'xtick.labelsize':  8,
          'ytick.labelsize':  8,
          'figure.dpi': 1000,
          'text.latex.preamble': r'\usepackage{siunitx} \usepackage{amsmath}'
          }


mpl.rcParams.update(params)

np.random.seed(123)

sn = SimpleOpenTubeNetwork(nnodes=10)
#sn.link_length *=
x0 = 0.5
sn.setup()
s = Scale()
nps = Particles(sn, 4000, s)
nps.particles["pos"] = x0
nps.particles["link"] = 5

nbins = 20
D = k_b * temp / (3 * np.pi * mu0 * d_p)
D_undim = s.undim_length(s.undim_velocity(D))

p0 = 10
p1 = 0

dP_per_l = 2.5e5 # Pa/m
dP = dP_per_l * sn.nlinks*s.l0*sn.link_length
dp = s.undim_pressure(dP)

p = solve_node_pressures(sn, dp, p1)
q = calculate_link_flows(sn, p, s.undim_viscosity(s.mu0))
u = q[0]/sn.link_crossections[0]

alpha = 100
dx_max = 0.1
dt = np.min([np.min(dx_max*sn.link_crossections[:]/(np.abs(q[:]))), dx_max**2/(2*D_undim)])/alpha

t = 0
iterations = 0


dx_adv = q[0] / sn.link_crossections[0] * dt
dx_diff = np.sqrt(2 * D_undim * dt)

for i in range(10):
    nps.move_particles(q, dt, D_undim)
    t += dt
    iterations += 1

t0 = t
iters0 = iterations
hist0_data = nps.particles['pos'].copy()


for i in range(100):
    nps.move_particles(q, dt, D_undim)
    t += dt
    iterations += 1

t1 = t
iters1 = iterations
hist1_data = nps.particles['pos'].copy()

for i in range(200):
    nps.move_particles(q, dt, D_undim)
    t += dt
    iterations += 1

t2 = t
iters2 = iterations
hist2_data = nps.particles['pos'].copy()

data = hist2_data.copy()

l_factor = int(u * t)
x = np.linspace(min(data), max(data), 100)
mean, std = norm.fit(data)
C = 1 / np.sqrt(4 * np.pi * D * t) * np.exp(-(x - u * t + l_factor) ** 2 / (4 * D * t))
fx = 1 / (std * np.sqrt(2 * np.pi)) * np.exp(-(x - mean) ** 2 / (2 * std ** 2))


width = 2.291
height = 2.376


y_max = 5
plt.figure(0)
plt.ylim((0, y_max))
plot_histogram_and_norms(hist0_data, nbins, iters0, t0, u, D_undim, x0)
fig = plt.gcf()
fig.set_size_inches(width, height)
plt.savefig(f'../../images/{s.dim_timestep(t0*1e6):.3f}_conv_diff.png', bbox_inches='tight')
plt.figure(1)
plt.ylim((0, y_max))
plot_histogram_and_norms(hist1_data, nbins, iters1, t1, u, D_undim, x0)
fig = plt.gcf()
fig.set_size_inches(width, height)
plt.savefig(f'../../images/{s.dim_timestep(t1*1e6):.3f}_conv_diff.png', bbox_inches='tight')
plt.figure(2)
plt.ylim((0, y_max))
plot_histogram_and_norms(hist2_data, nbins, iters2, t2, u, D_undim, x0)
fig = plt.gcf()
fig.set_size_inches(width, height)
plt.savefig(f'../../images/{s.dim_timestep(t2*1e6):.3f}_conv_diff.png', bbox_inches='tight')




