import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
from scipy.stats import binom

from npflow.constants import k_b, temp, mu0, d_p
from npflow.flows import calculate_link_flows
from npflow.networks import OpenHexagonalNetwork
from npflow.particles import Particles
from npflow.scaling import Scale
from npflow.solve_pressure import solve_node_pressures
from npflow.utils.network_plots import plot_nodes, plot_links
from npflow.utils.particle_plots import plot_particles


params = {'pgf.texsystem': 'pdflatex',
          'text.usetex': True,
          'font.family': 'serif',
          'figure.figsize':   (7, 5),
          'axes.labelsize':   21,
          'axes.titlesize':   21,
          'xtick.labelsize':  21,
          'ytick.labelsize':  21,
          'figure.dpi': 300,
          'text.latex.preamble': r'\usepackage{siunitx} \usepackage{amsmath}'
          }
mpl.rcParams.update(params)

np.random.seed(123)


def calc_binomial(network, nparticles):
    s = Scale(rho0=1000)
    nps = Particles(network, nparticles, s)

    p = solve_node_pressures(network, dp, p1)
    q = calculate_link_flows(network, p, s.undim_viscosity(s.mu0))

    link = 3
    nps.particles["link"] = link

    D = k_b * temp / (3 * np.pi * mu0 * d_p)
    D_undim = s.undim_length(s.undim_velocity(D)) * 1e-300

    dx_max = 0.5
    dt = np.min([np.min(dx_max * ohn.link_crossections[:] / (np.abs(q[:]))), dx_max ** 2 / (2 * D_undim)]) / 10

    for i in range(1240//5):
        nps.move_particles(q, dt, D_undim)

    x = nps.cartesian_coordinates[:, 0]
    results, edges, _ = plt.hist(x, bins=5, density=True, stacked=True)
    plt.clf()

    bin_width = edges[1] - edges[0]
    y_calc_binom = results * bin_width

    plt.bar((edges / bin_width - 1.75)[:-1], y_calc_binom, width=1, align='edge', edgecolor='black')
    plt.xlabel('x')
    plt.ylabel('Probability')


    x_binom = np.arange(0, 5)
    y_binom = binom.pmf(x_binom, n=4, p=0.5)
    plt.plot(x_binom, y_binom, 'ro')

    plt.savefig(f"../../images/binomial_n4_np{nparticles}.png", bbox_inches="tight")

    print(f"For {nparticles} particles:")
    print(f"Absolute errors are {(y_calc_binom-y_binom)*100:}%")
    print(f"Mean Squared Error: {np.sum((y_calc_binom-y_binom)**2)/len(y_binom)}")


p0 = 10
p1 = 0

s = Scale(rho0=1000)

np.random.seed(123)

ohn = OpenHexagonalNetwork(nx=10)
ohn.setup()

dP_per_l = 2.5e5 # Pa/m
dP = dP_per_l * np.max(ohn.node_positions[:, 1])*s.l0
dp = s.undim_pressure(dP)

p_nums = [200, 1000, 2000, 4000, 8000, 16000]

for num in p_nums:
    calc_binomial(ohn, num)

plt.show()
