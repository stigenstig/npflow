import matplotlib
import matplotlib.pyplot as plt

import platform

if platform.system() != 'Windows':
    matplotlib.rcParams['text.usetex'] = True


def plot_simple_pressure(network, pressure):

    y = network.node_positions[:, 1]
    p = pressure

    plt.plot(y, p, 'k.')
    plt.xlabel('y [m]')
    plt.ylabel('Pressure [Pa]')
    plt.show()
