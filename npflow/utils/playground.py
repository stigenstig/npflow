import matplotlib.pyplot as plt
import matplotlib as mpl
from pathlib import Path

import numpy as np
from scipy.stats import norm

from npflow.basecase import BaseCase, base_params

from npflow.utils.network_plots import plot_nodes, plot_links
from npflow.utils.particle_plots import plot_particles


params = {'pgf.texsystem': 'pdflatex',
          'text.usetex': True,
          'font.family': 'serif',
          'axes.labelsize':   10,
          'xtick.labelsize':  10,
          'ytick.labelsize':  10,
          'figure.dpi': 1000,
          'text.latex.preamble': r'\usepackage{siunitx} \usepackage{amsmath}'
          }


mpl.rcParams.update(params)

np.random.seed(123)

base_params["t_max"] = 150
base_case = BaseCase(params=base_params,ly=30, lx=14, N=400)

start_node = base_case.ohn.nnodes // 2 - 4 * base_case.ohn.nx + 1
end_node = start_node + base_case.ohn.nx

start_link = np.where(base_case.ohn.link_node_back == start_node)[0]
end_link = np.where(base_case.ohn.link_node_back == end_node)[0]
index_diff = 0

for particle in range(base_case.nps.nparticles):
    new_link = start_link + index_diff % (end_link - start_link)
    base_case.nps.particles[particle]['link'] = new_link
    index_diff += 3

nps_start_y = base_case.nps.cartesian_coordinates[:, 1].copy()

fig1 = plt.figure(0, dpi=300)

ax1 = plt.gca()

ax1 = plot_nodes(ax1, base_case.ohn, showtext=False)
ax1 = plot_links(ax1, base_case.ohn, scale=0.5)
ax1 = plot_particles(ax1, base_case.nps.cartesian_coordinates)

plt.savefig(Path.cwd().parent.parent / 'images' / 'basecase_plot_1.png', bbox_inches='tight')

fig2 = plt.figure(1, dpi=300)

ax2 = plt.gca()
ax2 = plot_nodes(ax2, base_case.ohn, showtext=False)
ax2 = plot_links(ax2, base_case.ohn, scale=0.5)

t = 0.0
t_max = base_case.params["t_max"]
t_max_undim = base_case.scaling.undim_timestep(t_max)
dt = base_case.dt
D_undim = base_case.D_undim
q = base_case.flow

while t < t_max_undim:
    if t + dt > t_max_undim:
        dt = t_max_undim - t
    base_case.nps.move_particles(q, dt, D_undim)
    t += dt

print(f"{np.sum(base_case.nps.particles['link'] == -1)} particles outside network")

ax2 = plot_particles(ax2, base_case.nps.cartesian_coordinates)
print("Did I plot the particles one more time?")

displacement = base_case.nps.cartesian_coordinates[:, 1]
displacement = base_case.scaling.dim_length(displacement-nps_start_y)
mean, std = norm.fit(displacement)
print(f"mean={mean*1e6:.3f}, std={std*1e6:.3f}")

print(np.sum(base_case.nps.particles['link'] == -1))

plt.savefig(Path.cwd().parent.parent / 'images' / 'basecase_plot_2.png', bbox_inches='tight')
