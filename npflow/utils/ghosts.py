import numpy as np
from npflow.networks import OpenHexagonalNetwork


def generate_ghost_nodes(network: OpenHexagonalNetwork):

    x = network.node_positions[:, 0]
    y = network.node_positions[:, 1]

    d = np.dtype([('index', 'i8'), ('pos', 'f', 2)])

    nghost_nodes = network.ny+2
    ghost_nodes = np.zeros(nghost_nodes, dtype=d)

    # first node has ghost twin
    ghost_nodes[0]['index'] = 0
    ghost_nodes[0]['pos'] = [x[0] + network.dx * network.nx, y[0]]

    # last node in top row has ghost twin
    index = network.nnodes - 1
    ghost_nodes[-1]['index'] = index
    ghost_nodes[-1]['pos'] = [x[index] - network.dx * network.nx, y[index]]


    # find remaining ghost nodes
    g_index = 1
    for node in range(network.nx//2+1, index-network.nx//2, 2):
        jx = (node - network.nx // 2) % network.nx
        jy = (node - network.nx // 2) // network.nx

        # Find if node has ghost twin that can be plotted
        if jy % 2 == 0:
            # Even row, first node has ghost node in last column
            if jx - 1 == 0:
                ghost_nodes[g_index]['index'] = node-1
                ghost_nodes[g_index]['pos'] = [x[node-1] + network.dx * network.nx, y[node-1]]
                g_index += 1
        else:
            # Odd row, node has ghost node in first column
            if jx == network.nx - 1:
                ghost_nodes[g_index]['index'] = node
                ghost_nodes[g_index]['pos'] = [x[node] - network.dx * network.nx, y[node]]
                g_index += 1

    return ghost_nodes
