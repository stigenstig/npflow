import numpy as np


def save_data_to_file(data, filename, header):
    if type(data) == dict:
        with open(filename, 'w') as f:
            f.write(f"#{header}\n")
            for key, value in data.items():
                f.write(f"{key:>10}: {value}\n")

    elif type(data) == np.ndarray:
        with open(filename, 'w') as f:
            f.write(f"#{header}\n")
            for i in range(len(data)):
                f.write(f"{data[i]}\n")


def save_latex_code_to_file(save_dir, param_key, latex_param, x, y):
    max_index = np.where(y == max(y))
    min_index = np.where(y == min(y))

    latex_keys = {
        "mu0":      "\kilo\pascal\second",
        "d_l":      "\\nano\meter",
        "dP_per_l": "\kilo\Pa\per\meter",
        "t_max":    "\second",
        "D_eff":    "\micro\meter\squared\per\second"
    }

    text = f"""
    \\begin{{figure}}
        \centering
        \\begin{{subfigure}}[t]{{0.95\linewidth}}
            \includegraphics[width=\\textwidth]{{params_analysis_q_{param_key}}}
            \caption{{The flow rate $q$ as function of ${latex_param}$.}}
            \label{{fig:q_{param_key}}}
        \end{{subfigure}}
        \\begin{{subfigure}}[]{{0.95\linewidth}}
            \includegraphics[width=\\textwidth]{{params_analysis_q_loglog_{param_key}}}
            \caption{{Log-log plot of the flow rate $q$ as function of ${latex_param}$.}}
            \label{{fig:q_loglog_{param_key}}}
        \end{{subfigure}}
        \caption{{Flow rate $q$ plots for ${latex_param}$.}}
    \end{{figure}}
    
    \\begin{{figure}}
        \centering
        \\begin{{subfigure}}[t]{{0.95\linewidth}}
            \includegraphics[width=\\textwidth]{{params_analysis_displacement_{param_key}}}
            \caption{{Particle penetration depth as function of ${latex_param}$. The penetration depth 
            reaches a maximum of $\mu_{{x}}=\SI{{{y[max_index][0]:.3f}}}{{\\micro\\meter}}$ at ${latex_param}=\SI{{{x[max_index][0]:.3f}}}{{{latex_keys[param_key]}}}$ 
            and a minimum of $\mu_{{x}}=\SI{{{y[min_index][0]:.3f}}}{{\\micro\\meter}}$ at ${latex_param}=\SI{{{x[min_index][0]:.3f}}}{{{latex_keys[param_key]}}}$.}}
            \label{{fig:ppd_{param_key}}}
        \end{{subfigure}}
        \\begin{{subfigure}}[]{{0.95\linewidth}}
            \includegraphics[width=\\textwidth]{{params_analysis_displacement_loglog_{param_key}}}
            \caption{{Log-log plot of particle penetration depth as function of ${latex_param}$.}}
            \label{{fig:ppd_loglog_{param_key}}}
        \end{{subfigure}}
        \caption{{Particle penetration depth plots for ${latex_param}$.}}
    \end{{figure}}
    
    \\begin{{figure}}
        \centering
        \\begin{{subfigure}}[t]{{0.95\linewidth}}
            \includegraphics[width=\\textwidth]{{params_analysis_std_{param_key}}}
            \caption{{Standard deviation $\sigma_{{x}}$ as function of ${latex_param}$.}}
            \label{{fig:std_{param_key}}}
        \end{{subfigure}}
        \\begin{{subfigure}}[]{{0.95\linewidth}}
            \includegraphics[width=\\textwidth]{{params_analysis_std_loglog_{param_key}}}
            \caption{{Log-log plot of standard deviation $\sigma_{{x}}$ as function of ${latex_param}$.}}
            \label{{fig:std_loglog_{param_key}}}
        \end{{subfigure}}
        \caption{{Standard deviation $\sigma_{{x}}$ plots for ${latex_param}$.}}
    \end{{figure}}

    \\begin{{figure}}
        \centering
        \\begin{{subfigure}}[t]{{0.95\linewidth}}
            \includegraphics[width=\\textwidth]{{params_analysis_re_{param_key}}}
            \caption{{The Reynolds number as function of ${latex_param}$.}}
            \label{{fig:re_{param_key}}}
        \end{{subfigure}}
        \\begin{{subfigure}}[]{{0.95\linewidth}}
            \includegraphics[width=\\textwidth]{{params_analysis_re_loglog_{param_key}}}
            \caption{{Log-log plot of the Reynolds number as function of ${latex_param}$.}}
            \label{{fig:re_loglog_{param_key}}}
        \end{{subfigure}}
        \caption{{Reynolds number plots for ${latex_param}$.}}
    \end{{figure}}
    
    \\begin{{figure}}
        \centering
        \\begin{{subfigure}}[t]{{0.95\linewidth}}
            \includegraphics[width=\\textwidth]{{params_analysis_pe_{param_key}}}
            \caption{{The Péclet number as function of ${latex_param}$.}}
            \label{{fig:pe_{param_key}}}
        \end{{subfigure}}
        \\begin{{subfigure}}[]{{0.95\linewidth}}
            \includegraphics[width=\\textwidth]{{params_analysis_pe_loglog_{param_key}}}
            \caption{{Log-log plot of the Péclet number as function of ${latex_param}$.}}
            \label{{fig:pe_loglog_{param_key}}}
        \end{{subfigure}}
        \caption{{Péclet number plots for ${latex_param}$.}}
    \end{{figure}}
    """

    with open(save_dir / 'latex_code.txt', 'w') as f:
        f.write(text)
