import numpy as np
import matplotlib.pyplot as plt
from npflow.utils.ghosts import generate_ghost_nodes
from npflow.utils.plots import plot_if_short
from npflow.networks import OpenHexagonalNetwork, SimpleOpenTubeNetwork
from npflow.scaling import Scale


def plot_simple_network(network: SimpleOpenTubeNetwork):
    fig = plt.figure(dpi=300)
    ax = fig.add_subplot(111)
    ax.set_aspect(1)
    link_color = 'gray'

    x = network.node_positions[:, 0]
    y = network.node_positions[:, 1]

    plt.xlim(min(x) - 0.5, max(x + network.dx) + 0.5)
    plt.ylim(min(y) - 0.5, max(y) + 0.5)
    plt.xlabel('column number')

    for node in network.nodes:
        ax.plot(x[node], y[node], 'ko')
        ax.text(x[node] + 0.04, y[node] + 0.04, s=str(node), fontsize='small')

    for link in range(network.nlinks):
        link_lw = 4 * network.link_radii[link] / np.max(network.link_radii[:])
        x0 = x[network.link_node_back[link]]
        x1 = x[network.link_node_front[link]]
        y0 = y[network.link_node_back[link]]
        y1 = y[network.link_node_front[link]]
        ax.plot([x0, x1], [y0, y1], color=link_color, linestyle='-', linewidth=link_lw, zorder=1)

    return ax


def plot_nodes(ax, network, showtext=True):

    ax.set_aspect(1)
    s = Scale()

    x = network.node_positions[:, 0]
    y = network.node_positions[:, 1]

    x_plot = np.unique(network.node_positions[:, 0])[::4]
    y_plot = np.unique(network.node_positions[:, 1])[::4]
    x_ticks = x_plot*s.l0*1e6
    y_ticks = y_plot*s.l0*1e6

    ax.set_xticks(x_plot)
    ax.set_xticklabels(x_ticks.astype(int))
    ax.set_yticks(y_plot)
    ax.set_yticklabels(y_ticks.astype(int))

    plt.xlim(min(x) - 0.5, max(x+network.dx) + 0.5)
    plt.ylim(min(y) - 0.5, max(y) + 0.5)
    plt.xlabel('x [\si{\micro\meter}]')
    plt.ylabel('y [\si{\micro\meter}]')

    for node in network.nodes:
        if showtext:
            ax.plot(x[node], y[node], 'ko')
        ax.text(x[node] + 0.2, y[node]-0.08, s=str(node) if showtext else '', fontsize='small')

    return ax


def plot_links(ax, network, scale=1):

    for link in range(network.nlinks):
        link_color = 'gray'
        plot_link_property(ax, network, link, network.link_radii, link_color, scale)

    return ax


def plot_link_flows(ax, network: OpenHexagonalNetwork, q):

    for link in range(network.nlinks):

        flow_color = 'red' if q[link] > 0 else 'blue'
        plot_link_property(ax, network, link, q, flow_color)

    return ax


def plot_link_property(ax, network, link, prop, l_color, scale):

    x = network.node_positions[:, 0]
    y = network.node_positions[:, 1]

    link_lw = 4 * prop[link]/np.max(prop)*scale
    x0 = x[network.link_node_back[link]]
    x1 = x[network.link_node_front[link]]
    y0 = y[network.link_node_back[link]]
    y1 = y[network.link_node_front[link]]

    if not isinstance(network, SimpleOpenTubeNetwork):

        ghost_nodes = generate_ghost_nodes(network)

        front_node = network.link_node_front[link]
        back_node = network.link_node_back[link]
        jx = (front_node - network.nx // 2) % network.nx
        jy = (front_node - network.nx // 2) // network.nx

        if jx == network.nx-1 and front_node-back_node == network.nx-1:
            if plot_if_short(x0+network.dx*network.nx, y0, x1, y1, ax, link_lw, l_color):
                network.link_positions[link] = [x0+network.dx*network.nx, y0, x1, y1]

        elif front_node in ghost_nodes['index'] and back_node in ghost_nodes['index']:
            if front_node-back_node == 1:
                if jy != network.ny:
                    if plot_if_short(x0, y0, x1 + network.dx * network.nx, y1, ax, link_lw, l_color):
                        network.link_positions[link] = [x0, y0, x1 + network.dx * network.nx, y1]

    if plot_if_short(x0, y0, x1, y1, ax, link_lw, l_color):
        network.link_positions[link] = [x0, y0, x1, y1]

    return ax

