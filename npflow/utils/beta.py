import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from pathlib import Path
import fnmatch

from scipy.stats import linregress

from npflow.basecase import BaseCase, base_params
from npflow.utils.latex_config import mpl_params, latex_param, latex_keys

mpl_params['figure.figsize'] = (12, 7)
mpl_params['axes.labelsize'] = 24
mpl_params['axes.titlesize'] = 24
mpl_params['xtick.labelsize'] = 24
mpl_params['ytick.labelsize'] = 24
mpl_params['legend.fontsize'] = 22

mpl.rcParams.update(mpl_params)

bc = BaseCase(base_params)

mu0 = base_params["mu0"]
d_l = base_params["d_l"]
dP_per_l = base_params["dP_per_l"]
D_eff = base_params["D"]


def calc_c(t_max, beta_ideal=False):
    results_dir = Path(f"/home/stig/PycharmProjects/npflow/data/t_{t_max}_new")
    params = ["mu0", "d_l", "dP_per_l", "D_eff", "t_max"]
    excluded_files = ["base_params", "mean_dist", "std_dist", 'latex_code.txt']

    C_m = C_s = 0

    if beta_ideal:
        bm_0 = -1
        bm_1 = 2
        bm_2 = 1
        bm_3 = 0
        bm_4 = 1

        bs_0 = -0.5
        bs_1 = 0
        bs_2 = 0
        bs_3 = 0.5
        bs_4 = 0.5
    else:
        if t_max == 300:
            bm_0 = -0.999
            bm_1 = 1.717
            bm_2 = 0.865
            bm_3 = 0
            bm_4 = 1.011

            bs_0 = -0.476
            bs_1 = 0
            bs_2 = 0
            bs_3 = 0.488
            bs_4 = 0.495

            results_dir = Path("/home/stig/Dropbox/NTNU/vår 2019/Masteroppgåve/used in thesis 2")

        elif t_max == 200:
            bm_0 = -1.0
            bm_1 = 1.936
            bm_2 = 0.936
            bm_3 = 0.0
            bm_4 = 1.042

            bs_0 = -0.506
            bs_1 = 0
            bs_2 = 0.016
            bs_3 = 0.492
            bs_4 = 0.480

            results_dir = Path("/home/stig/PycharmProjects/npflow/data/t_200")

        elif t_max == 150:

            bm_0 = -1.004
            bm_1 = 1.881
            bm_2 = 0.989
            bm_3 = 0
            bm_4 = 0.998

            bs_0 = -0.505
            bs_1 = 0
            bs_2 = 0
            bs_3 = 0.488
            bs_4 = 0.496

            results_dir = Path("/home/stig/PycharmProjects/npflow/data/t_150_new")

        elif t_max == 100:

            bm_0 = -1.001
            bm_1 = 1.900
            bm_2 = 0.966
            bm_3 = 0
            bm_4 = 1.026

            bs_0 = -0.506
            bs_1 = 0
            bs_2 = 0
            bs_3 = 0.490
            bs_4 = 0.492

            results_dir = Path("/home/stig/PycharmProjects/npflow/data/t_100_new")

        elif t_max == 50:

            bm_0 = -1.030
            bm_1 = 2.012
            bm_2 = 0.985
            bm_3 = 0
            bm_4 = 1.002

            bs_0 = -0.502
            bs_1 = 0
            bs_2 = 0
            bs_3 = 0.486
            bs_4 = 0.490

            results_dir = Path("/home/stig/PycharmProjects/npflow/data/t_50_new")

    for folder in results_dir.iterdir():
        file = [file for file in folder.iterdir() if file.name in params][0]
        x = np.loadtxt(Path(file.absolute()), delimiter='\n')
        param_key = file.name
        # print(f"param_key = {param_key}")
        # print(f"x={x}")
        c_calc = False
        mean_dist = np.loadtxt(Path(folder / 'mean_dist'), delimiter='\n')
        std_dist = np.loadtxt(Path(folder / 'std_dist'), delimiter='\n')

        if not c_calc:
            if param_key == "d_l":
                C_m = np.mean(mean_dist / (mu0 ** bm_0 * x ** bm_1 * dP_per_l ** bm_2 * D_eff ** bm_3 * t_max ** bm_4))
                C_s = np.mean(std_dist / (mu0 ** bs_0 * x ** bs_1 * dP_per_l ** bs_2 * D_eff ** bs_3 * t_max ** bs_4))
            elif param_key == "mu0":
                C_m = np.mean(mean_dist / (x ** bm_0 * d_l ** bm_1 * dP_per_l ** bm_2 * D_eff ** bm_3 * t_max ** bm_4))
                C_s = np.mean(std_dist / (x ** bs_0 * d_l ** bs_1 * dP_per_l ** bs_2 * D_eff ** bs_3 * t_max ** bs_4))
            elif param_key == "dP_per_l":
                C_m = np.mean(mean_dist / (mu0 ** bm_0 * d_l ** bm_1 * x ** bm_2 * D_eff ** bm_3 * t_max ** bm_4))
                C_s = np.mean(std_dist / (mu0 ** bs_0 * d_l ** bs_1 * x ** bs_2 * D_eff ** bs_3 * t_max ** bs_4))
            elif param_key == "D_eff":
                C_m = np.mean(mean_dist / (mu0 ** bm_0 * d_l ** bm_1 * dP_per_l ** bm_2 * x ** bm_3 * t_max ** bm_4))
                C_s = np.mean(std_dist / (mu0 ** bs_0 * d_l ** bs_1 * dP_per_l ** bs_2 * x ** bs_3 * t_max ** bs_4))
            elif param_key == "t_max":
                C_m = np.mean(mean_dist / (mu0 ** bm_0 * d_l ** bm_1 * dP_per_l ** bm_2 * D_eff ** bm_3 * x ** bm_4))
                C_s = np.mean(std_dist / (mu0 ** bs_0 * d_l ** bs_1 * dP_per_l ** bs_2 * D_eff ** bs_3 * x ** bs_4))
        c_calc = True

    return C_m, C_s


def exp_transport(C_m, C_s, t_max, t_sim, beta_ideal=False):
    bm_0 = bm_1 = bm_2 = bm_3 = bm_4 = 0
    bs_0 = bs_1 = bs_2 = bs_3 = bs_4 = 0

    if beta_ideal:
        bm_0 = -1
        bm_1 = 2
        bm_2 = 1
        bm_3 = 0
        bm_4 = 1

        bs_0 = -0.5
        bs_1 = 0
        bs_2 = 0
        bs_3 = 0.5
        bs_4 = 0.5
    else:
        print(f"t_max={t_max}")

        if t_max == 300:
            bm_0 = -0.999
            bm_1 = 1.717
            bm_2 = 0.865
            bm_3 = 0
            bm_4 = 1.011

            bs_0 = -0.476
            bs_1 = 0
            bs_2 = 0
            bs_3 = 0.488
            bs_4 = 0.495

        elif t_max == 200:
            bm_0 = -1.0
            bm_1 = 1.936
            bm_2 = 0.936
            bm_3 = 0.0
            bm_4 = 1.042

            bs_0 = -0.506
            bs_1 = 0
            bs_2 = 0.016
            bs_3 = 0.492
            bs_4 = 0.480

        elif t_max == 150:
            bm_0 = -1.004
            bm_1 = 1.881
            bm_2 = 0.989
            bm_3 = 0
            bm_4 = 0.998

            bs_0 = -0.505
            bs_1 = 0
            bs_2 = 0
            bs_3 = 0.488
            bs_4 = 0.496

        elif t_max == 100:
            bm_0 = -1.001
            bm_1 = 1.900
            bm_2 = 0.966
            bm_3 = 0
            bm_4 = 1.026

            bs_0 = -0.506
            bs_1 = 0
            bs_2 = 0
            bs_3 = 0.490
            bs_4 = 0.492

        elif t_max == 50:
            bm_0 = -1.030
            bm_1 = 2.012
            bm_2 = 0.985
            bm_3 = 0
            bm_4 = 1.002

            bs_0 = -0.502
            bs_1 = 0
            bs_2 = 0
            bs_3 = 0.486
            bs_4 = 0.490

    print(bm_0)

    t_exp = np.linspace(0, t_sim, 100)

    mu_exp = C_m * mu0 ** bm_0 * d_l ** bm_1 * dP_per_l ** bm_2 * D_eff ** bm_3 * t_exp ** bm_4
    sigma_exp = C_s * mu0 ** bs_0 * d_l ** bs_1 * dP_per_l ** bs_2 * D_eff ** bs_3 * t_exp ** bs_4

    return mu_exp, sigma_exp, t_exp


def calc_adv_diff_from_data(folder):
    D_aq = 7.568720150606439e-12
    mean = std = t = 0

    dir = Path(folder)
    mean = np.loadtxt(dir / 'mean_dist', delimiter='\n')
    std = np.loadtxt(dir / 'std_dist', delimiter='\n')
    t = np.loadtxt(dir / 't_max', delimiter='\n')

    mean_log = np.log10(mean)
    std_log = np.log10(std)
    t_log = np.log10(t)

    slope_s, intercept_s, r_value_s, p_value_s, std_err_s = linregress(t_log, std_log)
    D = 10 ** (2 * intercept_s - np.log10(2))

    slope_m, intercept_m, r_value_m, p_value_m, std_err_m = linregress(t_log, mean_log)
    u = 10 ** intercept_m

    print(f"For folder {folder}")
    print(f"a={intercept_s}")
    print(f"b={slope_s}")
    print(f"D={D*1e12:.3f}")
    print(f"a={intercept_m}")
    print(f"b={slope_m}")
    print(f"u={u*1e6:.3f}")
    print(f"tort={np.sqrt(D_aq/D)}")

    return u, D, slope_m, slope_s


def plot_t_300():
    C_m, C_s = calc_c(300)
    results_dir = Path("/home/stig/PycharmProjects/npflow/data/t_300/2019.05.28_22:19:18_t_max")
    mu = np.loadtxt(Path(results_dir / "mean_dist"))
    std = np.loadtxt(Path(results_dir / "std_dist"))
    t = np.loadtxt(Path(results_dir / "t_max"))
    mu_exp, sigma_exp, t_exp = exp_transport(C_m, C_s, 300, t.max())
    plt.errorbar(t, y=mu * 1e6, yerr=std * 1e6, fmt='-o', label='Simulated')
    plt.plot(t_exp, mu_exp * 1e6, c='k', label='Extrapolated')
    plt.plot(t_exp, (mu_exp + sigma_exp) * 1e6, '.', c='k')
    plt.plot(t_exp, (mu_exp - sigma_exp) * 1e6, '.', c='k')

    plt.xlabel(f"${latex_param['t_max']} {latex_keys['t_max']}$")
    plt.ylabel("Particle penetration depth $[\si{\micro\meter}$]")
    plt.legend()
    plt.show()


def plot_estimates():
    plt.figure(1)
    times = [50, 100, 150]
    t_sim = 300
    colors = ['k', 'b', 'r']
    c = 0
    for t in times:
        print(t)
        C_m, C_s = calc_c(t)
        mu_exp, sigma_exp, t_exp = exp_transport(C_m, C_s, t, t_sim)
        u, D, slope_m, slope_s = calc_adv_diff_from_data(folders[c])
        plt.figure(1)
        plt.plot(t_exp, mu_exp * 1e6, label=f"${latex_param['t_max']}=\SI{{{t}}}{{\second}}$", c=colors[c])
        plt.plot(t_exp, (mu_exp + sigma_exp) * 1e6, '.', c=colors[c])
        plt.plot(t_exp, (mu_exp - sigma_exp) * 1e6, '.', c=colors[c])
        plt.figure(2)
        mu_calc = u * t_exp ** slope_m
        sigma_calc = np.sqrt(2 * D) * t_exp ** slope_s
        #plt.plot(t_exp, mu_calc * 1e6, label=f"${latex_param['t_max']}=\SI{{{t}}}{{\second}}$", c=colors[c])
        #plt.plot(t_exp, (mu_calc + sigma_calc) * 1e6, '.', c=colors[c])
        #plt.plot(t_exp, (mu_calc - sigma_calc) * 1e6, '.', c=colors[c])
        #plt.clf()
        c += 1

    folder = Path("/home/stig/Dropbox/NTNU/vår 2019/Masteroppgåve/used in thesis 2/2019.05.28_22:19:18_t_max")
    mu = np.loadtxt(Path(folder / 'mean_dist'), delimiter='\n')
    std = np.loadtxt(Path(folder / 'std_dist'), delimiter='\n')
    t = np.loadtxt(Path(folder / 't_max'), delimiter='\n')
    t = t[np.where(t < 300)]
    plt.figure(1)
    plt.errorbar(t, mu[:len(t)] * 1e6, yerr=std[:len(t)] * 1e6, fmt='-o',
                 label=f"${latex_param['t_max']}=\SI{{300}}{{\second}}$")

    plt.xlabel(f"$t {latex_keys['t_max']}$")
    plt.ylabel("Particle penetration depth $[\si{\micro\meter}$]")
    plt.legend()
    """
    u, D, slope_m, slope_s = calc_adv_diff_from_data(folder)
    plt.figure(2)
    # plt.errorbar(t, mu[:len(t)] * 1e6, yerr=std[:len(t)] * 1e6, fmt='-o', label='300')
    plt.errorbar(t, 1e6 * u * t ** slope_m, yerr=np.sqrt(2 * D) * t ** slope_s * 1e6,
                 label=f"${latex_param['t_max']}=\SI{{300}}{{\second}}$", fmt='-o')
    plt.xlabel(f"$t\ {latex_keys['t_max']}$")
    plt.ylabel("Particle penetration depth $[\si{\micro\meter}$]")
    plt.legend()
    """

    plt.show()


folders = [
    "/home/stig/PycharmProjects/npflow/data/t_50_new/2019.05.27_15:37:51_t_max",
    # "/home/stig/PycharmProjects/npflow/data/t_100_new/2019.05.27_23:39:17_t_max",
    "/home/stig/PycharmProjects/npflow/data/2019.05.28_14:24:14_t_max",
    "/home/stig/PycharmProjects/npflow/data/t_150_new/2019.05.27_21:43:07_t_max",
    "/home/stig/Dropbox/NTNU/vår 2019/Masteroppgåve/used in thesis 2/2019.05.28_22:19:18_t_max"
]

plot_estimates()

"""
for folder in folders:
    calc_adv_diff_from_data(folder)
"""
