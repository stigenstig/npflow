
class SizeNotEvenException(Exception):
    pass


class SmallSizeException(Exception):
    pass
