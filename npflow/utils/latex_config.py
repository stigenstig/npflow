mpl_params = {'pgf.texsystem':       'pdflatex',
              'text.usetex':         True,
              'font.family':         'serif',
              'figure.figsize':      (13, 5),
              'axes.labelsize':      22,
              'axes.titlesize':      22,
              'xtick.labelsize':     22,
              'ytick.labelsize':     22,
              'legend.fontsize':     20,
              'figure.dpi':          300,
              'text.latex.preamble': r'\usepackage{siunitx} \usepackage{amsmath}',
              "pgf.preamble":        [
                  r"\usepackage{siunitx}",
                  r"\usepackage{amsmath}"
              ]  # using this preamble
              }

latex_param = {
    "mu0":      "\mu_{0}",
    "d_l":      "d_{l}",
    "dP_per_l": "\Delta P_{u}/L_{y}",
    "t_max":    "t_{\mathrm{max}}",
    "log10":    "\log_{10}",
    'D_eff':    'D'
}
latex_keys = {
    "mu0":      "[\si{\kilo\pascal\second}]",
    "d_l":      "[\si{\\nano\meter}]",
    "dP_per_l": "[\si{\kilo\Pa\per\meter}]",
    "t_max":    "[\si{\second}]",
    "D_eff":    "[\si{\micro\meter\squared\per\second}]"
}