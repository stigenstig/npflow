import numpy as np

# directions
ix, iy, iz = 0, 1, 2

# tissue parameters
mu0 = 1.2e-3                            # interstitial fluid viscosity [] = Pa s
d_c = 10e-6                             # cell diameter [] = m
l0 = d_c / 2                            # link length   [] = m
#d_l = 500e-9                            # link diameter [] = m
d_l = 200e-9
r0 = d_l / 2                            # link radius   [] = m
r_l_ratio = r0/l0                       # link radius to length ratio [] = 1
rho0 = 1000                           # density of interstitial space
k_b = 1.38064852e-23                    # Boltzmann constant [] = m^2 kg/s^2
d_p = 50e-9                             # particle diameter [] = m
temp = 310.0                            # temperature in system [] = K
MI = 0.2                                # Mechanical index [] = 1
f0 = 500e3                              # US frequency [] = Hz



