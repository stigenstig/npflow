import gc
import numpy as np
from datetime import datetime
from pathlib import Path

from npflow.run import run_job
from npflow.utils.plots import plot_params
from npflow.utils.filesave import save_data_to_file
from npflow.basecase import base_params

np.random.seed(123)

base_params["t_max"] = 300
base_params["D_eff"] *= 1
base_params["npoints"] = 10
base_params["mu0"] *= 1e3
base_params["dP_per_l"] *= 44

runs = {
    "mu0":      [0.6, 1.4],
    "d_l":      [0.6, 1.4],
    "dP_per_l": [0.5, 1.5],
    "t_max":    [0.1, 1.5],
    "D_eff":    [0.3, 1.2]
}


def simulation_run(params, param_key, param_values):
    npoints = params["npoints"]

    mean_dist, std_dist, pe, re, q_tot = run_job(params, param_key, npoints, param_values)

    params["D_eff"] = params["D"]
    base_param = params[param_key]

    main_path = Path.cwd()
    data_path = main_path.parent / 'data'
    timestamp_now = datetime.now()
    run_dir = timestamp_now.strftime("%Y.%m.%d_%H:%M:%S") + '_' + param_key

    save_dir = data_path / run_dir
    if not save_dir.is_dir():
        save_dir.mkdir()

    print(f'Created folder: {save_dir}')

    mean_dist = np.array(mean_dist)
    std_dist = np.array(std_dist)
    q_tot = np.array(q_tot)
    pe = np.array(pe)
    re = np.array(re)
    plot_params(param_values * base_param, mean_dist, std_dist, param_key, pe, re, q_tot, save_dir)

    save_data_to_file(mean_dist,
                      save_dir / 'mean_dist',
                      "Mean particle displacement in meter")

    save_data_to_file(std_dist,
                      save_dir / 'std_dist',
                      "Standard deviation particle displacement in meter")

    save_data_to_file(param_values * base_param,
                      save_dir / param_key,
                      f'The {param_key} used in this run')

    save_data_to_file(base_params,
                      save_dir / 'base_params',
                      "Base parameters")


params = ["t_max"]

for param_key in params:
    values = runs[param_key]
    print(f"{param_key} has value {base_params[param_key]}")
    npoints = base_params["npoints"]
    parameter_values = np.zeros(npoints)
    parameter_values = np.linspace(values[0], values[1], npoints)
    simulation_run(base_params, param_key, parameter_values)
    gc.collect()
