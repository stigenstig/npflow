import numpy as np
from abc import ABC, abstractmethod

from npflow.constants import ix, iy, r_l_ratio
from npflow.custom_types import NodeTypes, LinkTypes
from npflow.utils.exceptions import SizeNotEvenException, SmallSizeException


class Network(ABC):
    r"""
    Abstract base class for a porous network
    """

    def __init__(self, random_volume=False):
        self.random_volume = random_volume
        self.link_length = 1
        self.link_radius = r_l_ratio * self.link_length
        self.volume = 0

    @abstractmethod
    def setup(self):
        pass

    @abstractmethod
    def setup_nodes(self):
        pass

    @abstractmethod
    def setup_links(self):
        pass

    @abstractmethod
    def setup_node_positions(self):
        pass

    @abstractmethod
    def setup_volume(self):
        pass


class OpenHexagonalNetwork(Network):
    r"""
    The following is an example structure with nx = 6 and ny = 4.

        0 1 2 3 4 5       Column numbers

        0   1   2   0
     ---|---|---|---|---  Row numbers
        3   5   7   3
         \ / \ / \ /      0
          4   6   8
     -----|---|---|-----
          9  11  13
         / \ / \ / \      1
        14  10  12  14
     ---|---|---|---|---
        15  17  19  15
         \ / \ / \ /      2
          16  18  20
     -----|---|---|-----
          21  23  25
         / \ / \ / \      3
        26  22  24  26
     ---|---|---|---|---
        27  28  29  27

    """

    def __init__(self, nx=6, ny=4, random_volume=False):
        self.nx, self.ny = nx, ny

        if self.nx % 2 != 0:
            raise SizeNotEvenException('Error: nx must be even!')

        if self.ny % 2 != 0:
            raise SizeNotEvenException('Error: ny must be even!')

        if self.nx < 6:
            raise SmallSizeException('Error: nx must be set to 6 or larger!')

        if self.ny < 4:
            raise SmallSizeException('Error: ny must be set to 4 or larger!')

        # Each node is connected to 3 other nodes
        self.nneighbours = 3

        # The term n+1 comes from the extra rows at top and bottom
        self.nnodes = nx * (ny + 1)

        # All nodes in interior connected to 3 links + top links
        self.nlinks = int(self.nneighbours * nx * ny / 2 + nx / 2)

        self.dx = np.cos(np.pi / 6.0)
        self.dy = (1.0 + np.sin(np.pi / 6.0))
        self.nodes = np.arange(0, self.nnodes)
        self.node_type = np.zeros(self.nnodes)

        # array where index n determines number of neighbours of the nth node
        self.node_nneighbours = np.zeros(self.nnodes, dtype='i8')

        # array thats sets the index of neighbouring nodes
        # first index in x is front neighbour, second back
        self.node_node_neighbours = np.zeros((self.nnodes, self.nneighbours), dtype='i8')

        # array of node positions in cartesian coordinates
        self.node_positions = np.zeros((self.nnodes, 2))
        self.link_positions = np.zeros((self.nlinks, 4))

        self.link_crossections = np.zeros(self.nlinks)
        self.link_volumes = np.zeros(self.nlinks)
        self.link_radii = np.zeros(self.nlinks)
        self.link_type = np.zeros(self.nlinks)

        # Determines which links the node is connected to
        # Index 0 front, index 1 back
        self.node_link_neighbours = np.zeros((self.nnodes, self.nneighbours), dtype='i8')

        # Specifies which nodes in a link that are front or back
        self.link_node_front = np.zeros(self.nlinks, dtype='i8')
        self.link_node_back = np.zeros(self.nlinks, dtype='i8')

        super(OpenHexagonalNetwork, self).__init__(random_volume)

    def setup(self):
        self.setup_nodes()
        self.setup_links()
        self.setup_node_positions()
        self.setup_link_positions()
        self.setup_volume()

    def setup_nodes(self):
        # Interior nodes have 3 neighbours
        self.node_nneighbours[self.nx // 2:self.nnodes - self.nx // 2] = self.nneighbours
        # Top and bottom nodes have 1 neighbour
        self.node_nneighbours[:self.nx // 2] = 1
        self.node_nneighbours[self.nnodes - self.nx // 2:] = 1

        # Setup first row
        jx = 0
        for node in np.arange(0, self.nx // 2):
            self.node_node_neighbours[node, 0] = self.nx // 2 + jx
            # Set up node as corresponding neighbour, for the neighbour
            self.node_node_neighbours[self.node_node_neighbours[node, 0], 0] = node

            # Node type is inlet
            self.node_type[node] = NodeTypes.NODE_INLET.value

            jx += 2

        # Setup normal rows
        for node in np.arange(self.nx // 2 + 1, self.nnodes - self.nx // 2, 2):
            jx = (node - self.nx // 2) % self.nx
            jy = (node - self.nx // 2) // self.nx

            # Even-numbered node :
            #   Always connected to node-1.
            self.node_node_neighbours[node, 2] = node - 1
            #   Always connected to node+1, else node+1-nx at end of row.
            if jx != self.nx - 1:
                self.node_node_neighbours[node, 1] = node + 1
            else:
                self.node_node_neighbours[node, 1] = node + 1 - self.nx

            # Vertical links:
            if jy % 2 == 0:
                # Even row, connected to i-1+nx
                self.node_node_neighbours[node, 0] = node - 1 + self.nx
            else:
                # Odd row
                if jx == self.nx - 1 and jy != self.ny - 1:
                    # connected to i+1 if at end of row.
                    self.node_node_neighbours[node, 0] = node + 1
                    # print(node)
                elif jy == self.ny - 1:
                    # Last row connected to outlet row, nnodes-nx//2+jx//2
                    neigh = self.nnodes - self.nx // 2 + jx // 2
                    self.node_node_neighbours[node, 0] = neigh
                else:
                    # connected to i+1+nx
                    self.node_node_neighbours[node, 0] = node + 1 + self.nx

            # Connect odd numbered nodes back to this node
            self.node_node_neighbours[self.node_node_neighbours[node, 0], 0] = node
            self.node_node_neighbours[self.node_node_neighbours[node, 1], 1] = node
            self.node_node_neighbours[self.node_node_neighbours[node, 2], 2] = node

            # Normal node types
            self.node_type[node] = NodeTypes.NODE_NORMAL.value
            self.node_type[node - 1] = NodeTypes.NODE_NORMAL.value

        # Top row types
        self.node_type[self.nnodes - self.nx // 2:] = NodeTypes.NODE_OUTLET.value

    def setup_links(self):
        # Setup links from first row
        for node in np.arange(0, self.nx // 2):
            link = node
            # Connect link to nodes, i.e. link 0 to nodes 0 and 3
            self.node_link_neighbours[node, 0] = link
            # Neighbouring node is connected to the same link.
            self.node_link_neighbours[self.node_node_neighbours[node, 0], 0] = link

            # Connect front and back nodes to link
            self.link_node_back[link] = node
            self.link_node_front[link] = self.node_node_neighbours[node, 0]

        # Iterate over normal rows
        for node in np.arange(self.nx // 2 + 1, self.nnodes - self.nx // 2, 2):
            self.node_link_neighbours[node, 2] = ((node - 1 - self.nx // 2) // 2) * 3 + \
                                                 0 + self.nx // 2
            self.node_link_neighbours[node, 1] = ((node - 1 - self.nx // 2) // 2) * 3 + \
                                                 1 + self.nx // 2
            self.node_link_neighbours[node, 0] = ((node - 1 - self.nx // 2) // 2) * 3 + \
                                                 2 + self.nx // 2

            self.node_link_neighbours[self.node_node_neighbours[node, 0], 0] = \
                self.node_link_neighbours[node, 0]
            self.node_link_neighbours[self.node_node_neighbours[node, 1], 1] = \
                self.node_link_neighbours[node, 1]
            self.node_link_neighbours[self.node_node_neighbours[node, 2], 2] = \
                self.node_link_neighbours[node, 2]

            self.link_node_front[self.node_link_neighbours[node, 2]] = node
            self.link_node_back[self.node_link_neighbours[node, 2]] = \
                self.node_node_neighbours[node, 2]
            self.link_node_front[self.node_link_neighbours[node, 1]] = node
            self.link_node_back[self.node_link_neighbours[node, 1]] = \
                self.node_node_neighbours[node, 1]
            self.link_node_front[self.node_link_neighbours[node, 0]] = \
                self.node_node_neighbours[node, 0]
            self.link_node_back[self.node_link_neighbours[node, 0]] = node

        for link in range(self.nlinks):
            if self.node_type[self.link_node_back[link]] == NodeTypes.NODE_INLET.value:
                self.link_type[link] = LinkTypes.LINK_INLET.value
            elif self.node_type[self.link_node_front[link]] == NodeTypes.NODE_OUTLET.value:
                self.link_type[link] = LinkTypes.LINK_OUTLET.value
            else:
                self.link_type[link] = LinkTypes.LINK_NORMAL.value

    def setup_node_positions(self):
        jx = 0
        # First row
        for node in np.arange(0, self.nx // 2):
            self.node_positions[node, ix] = jx * self.dx
            self.node_positions[node, iy] = 0.0
            jx += 2

        # Normal nodes
        for node in np.arange(self.nx // 2, self.nnodes - self.nx // 2):
            jx = (node - self.nx // 2) % self.nx
            jy = (node - self.nx // 2) // self.nx

            if jy % 2 == 0:
                # Even row
                self.node_positions[node, ix] = jx * self.dx
            else:
                # odd row, nodes starts in second column
                self.node_positions[node, ix] = (jx + 1) * self.dx

            if jx % 2 == 0:
                # even column
                self.node_positions[node, iy] = jy * self.dy + 2 / 3 * self.dy
            else:
                # odd column
                self.node_positions[node, iy] = jy * self.dy + np.tan(np.pi / 6) + 2 / 3 * self.dy

        # Last row
        jx = 0
        for node in np.arange(self.nnodes - self.nx // 2, self.nnodes):
            self.node_positions[node, ix] = (jx + 2) * self.dx
            self.node_positions[node, iy] = self.dy * (self.ny + 2 / 3)
            jx += 2

    def setup_link_positions(self):
        from npflow.utils.ghosts import generate_ghost_nodes

        x = self.node_positions[:, 0]
        y = self.node_positions[:, 1]
        ghost_nodes = generate_ghost_nodes(self)
        for link in range(self.nlinks):
            x0 = x[self.link_node_back[link]]
            x1 = x[self.link_node_front[link]]
            y0 = y[self.link_node_back[link]]
            y1 = y[self.link_node_front[link]]
            front_node = self.link_node_front[link]
            back_node = self.link_node_back[link]
            jx = (front_node - self.nx // 2) % self.nx
            jy = (front_node - self.nx // 2) // self.nx

            if jx == self.nx - 1 and front_node - back_node == self.nx - 1:
                if np.sqrt((x0 + self.dx * self.nx-x1)**2 + (y0-y1)**2) < 1.1:
                    self.link_positions[link] = [x0 + self.dx * self.nx, y0, x1, y1]

            elif front_node in ghost_nodes['index'] and back_node in ghost_nodes['index']:
                if front_node - back_node == 1:
                    if jy != self.ny:
                        if np.sqrt((x0-(x1 + self.dx * self.nx))**2 + (y0-y1)**2):
                            self.link_positions[link] = [x0, y0, x1 + self.dx * self.nx, y1]

            if np.sqrt((x0-x1)**2+(y0-y1)**2) < 1.1:
                self.link_positions[link] = [x0, y0, x1, y1]

    def setup_volume(self):

        if not self.random_volume:
            self.link_radii[:] = self.link_radius
        else:
            # TODO: Find a link radius distribution that fits with actual cells
            self.link_radii[:] = [(0.5 + np.random.random()) * self.link_radius for x in range(self.nlinks)]
        self.link_crossections[:] = np.pi * self.link_radii[:] ** 2
        self.link_volumes[:] = self.link_crossections[:] * self.link_length
        self.volume = np.sum(self.link_volumes)


class SimpleOpenTubeNetwork(Network):
    """
    Set up a simple open single-tube network.

            3
            |   2
            2
            |   1
            1
            |   0
            0
    """

    def __init__(self, nnodes=4, random_volume=False):
        self.nneighbours = 2
        self.nnodes = nnodes
        self.nlinks = self.nnodes - 1
        self.dy = 1
        self.dx = 0
        self.nx = 1
        self.nodes = np.arange(0, self.nnodes)
        self.node_type = np.zeros(self.nnodes)

        # array where index n determines neighbours of the nth node
        self.node_nneighbours = np.zeros(self.nnodes, dtype='i8')

        # array thats sets the index of neighbouring nodes
        # first index in x is front neighbour, second back
        self.node_node_neighbours = np.zeros((self.nnodes, self.nneighbours), dtype='i8')

        # array of node and link positions in cartesian coordinates
        self.node_positions = np.zeros((self.nnodes, 2))
        self.link_positions = np.zeros((self.nlinks, 4))

        self.link_crossections = np.zeros(self.nlinks)
        self.link_volumes = np.zeros(self.nlinks)
        self.link_radii = np.zeros(self.nlinks)
        self.link_type = np.zeros(self.nlinks)

        # Determines which links the node is connected to
        # Index 0 front, index 1 back
        self.node_link_neighbours = np.zeros((self.nnodes, 2), dtype='i8')

        # Specifies which nodes in a link that are front or back
        self.link_node_front = np.zeros(self.nlinks, dtype='i8')
        self.link_node_back = np.zeros(self.nlinks, dtype='i8')

        super(SimpleOpenTubeNetwork, self).__init__(random_volume)

    def setup(self):
        self.setup_nodes()
        self.setup_links()
        self.setup_node_positions()
        self.setup_volume()

    def setup_nodes(self):

        for node in self.nodes:
            if node == 0:  # first node
                self.node_node_neighbours[node] = 1
                self.node_node_neighbours[node, 0] = 1
                self.node_type[node] = NodeTypes.NODE_INLET.value
            elif node == self.nodes[-1]:
                self.node_node_neighbours[node] = 1
                self.node_node_neighbours[node, 0] = node - 1
                self.node_type[node] = NodeTypes.NODE_OUTLET.value
            else:  # normal node
                self.node_nneighbours[node] = self.nneighbours
                self.node_node_neighbours[node, 0] = node + 1
                self.node_node_neighbours[node, 1] = node - 1
                self.node_type[node] = NodeTypes.NODE_NORMAL.value

    def setup_links(self):

        for link in range(self.nlinks):
            # all nodes of index i is connected to link i
            node = link
            self.node_link_neighbours[node, 0] = link
            self.link_node_back[link] = node
            node += 1
            self.node_link_neighbours[node, 1] = link
            self.link_node_front[link] = node
            # node of index j+1 is always connected to node j
            if self.node_type[self.link_node_back[link]] == NodeTypes.NODE_INLET.value:
                self.link_type[link] = LinkTypes.LINK_INLET.value
            elif self.node_type[self.link_node_front[link]] == NodeTypes.NODE_OUTLET.value:
                self.link_type[link] = LinkTypes.LINK_OUTLET.value
            else:
                self.link_type[link] = LinkTypes.LINK_NORMAL.value

    def setup_volume(self):
        if self.random_volume:
            self.link_radii[:] = [(0.5 + np.random.random()) * self.link_radius for x in range(self.nlinks)]
        else:
            self.link_radii[:] = self.link_radius
        self.link_crossections[:] = np.pi * self.link_radii[:] ** 2
        self.link_volumes[:] = self.link_crossections[:] * self.link_length
        self.volume = np.sum(self.link_volumes)

    def setup_node_positions(self):
        self.node_positions[:, iy] = [x * self.dy for x in range(self.nnodes)]
        self.node_positions[:, ix] = 0


class SimpleOpenDoubleTubeNetwork(object):
    r"""
        Set up a simple open single-tube network.

                5
                |   3
                4
               / \   2
              2   3
               \ /   1
                1
                |   0
                0
        """

    def __init__(self):
        self.nneighbours = 3
        self.nnodes = 6
        self.nlinks = 6
        self.dy = 1
        self.nodes = np.arange(0, self.nnodes)

    """    
        self.setup_nodes()
        self.setup_links()
        
    def setup_nodes(self):
        # array where index n determines neighbours of the nth node
        self.node_nneighbours = np.zeros(self.nnodes)

        # array thats sets the index of neighbouring nodes
        # first index in x is front neighbour, second back
        self.node_node_neighbours = np.zeros((self.nnodes, self.nneighbours))

        node = 0
        self.node_nneighbours[node] = 1
        self.node_node_neighbours[node, 0] = 1

        node = 1
        self.node_nneighbours[node] = self.nneighbours
        self.node_node_neighbours[node, 0] = 2
        self.node_node_neighbours[node, 1] = 0

        node = 2
        self.node_nneighbours[node] = self.nneighbours
        self.node_node_neighbours[node, 0] = 3
        self.node_node_neighbours[node, 1] = 1

        node = 3
        self.node_nneighbours[node] = 1
        self.node_node_neighbours[node, 0] = 2

        # array of node positions in cartesian coordinates
        self.node_positions = np.zeros((self.nnodes, 2))
        self.node_positions[0, iy] = 0.0
        self.node_positions[1, iy] = self.dy
        self.node_positions[2, iy] = 2 * self.dy
        self.node_positions[3, iy] = 3 * self.dy

        self.node_type = np.zeros(self.nnodes)
        self.node_type[0] = NodeTypes.NODE_INLET.value
        self.node_type[1:2] = NodeTypes.NODE_NORMAL.value
        self.node_type[3] = NodeTypes.NODE_OUTLET.value

    def setup_links(self):
        self.link_radius = 0.1 * self.dy
        self.link_length = self.dy

        # Determines which links the node is connected to
        # Index 0 front, index 1 back
        self.node_link_neighbours = np.zeros((self.nnodes, 2))

        self.node_link_neighbours[0, 0] = 0

        self.node_link_neighbours[1, 0] = 1
        self.node_link_neighbours[1, 1] = 0

        self.node_link_neighbours[2, 0] = 2
        self.node_link_neighbours[2, 1] = 1

        # TODO: Check if the indexing of this is correct
        self.node_link_neighbours[3, 0] = 2

        # Specifies which nodes in a link that are front or back
        self.link_node_front = np.zeros(self.nlinks)
        self.link_node_back = np.zeros(self.nlinks)

        # link 0 has node 1 in front, 0 in back
        self.link_node_front[0] = 1
        self.link_node_back[0] = 0

        self.link_node_front[1] = 2
        self.link_node_back[1] = 1

        self.link_node_front[2] = 3
        self.link_node_back[2] = 2

        self.link_type = np.zeros(self.nlinks)
        self.link_type[:] = LinkTypes.LINK_NORMAL.value
    """
