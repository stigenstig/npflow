import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import norm

from npflow.constants import k_b, temp, mu0, d_p, rho0, d_c, l0, d_l, f0, MI
from npflow.flows import calculate_link_flows
from npflow.networks import SimpleOpenTubeNetwork, OpenHexagonalNetwork
from npflow.particles import Particles
from npflow.scaling import Scale
from npflow.solve_pressure import solve_node_pressures, solve_elastic_node_pressures
from npflow.utils.network_plots import plot_nodes, plot_links
from npflow.utils.particle_plots import plot_particles
from npflow.utils.plots import plot_histogram_and_norms

from npflow.networks import SimpleOpenTubeNetwork, SimpleOpenDoubleTubeNetwork, OpenHexagonalNetwork, Network
from npflow.custom_types import NodeTypes
from npflow.flows import calculate_link_mobilities
from npflow.scaling import Scale

base_params = {
    "N":        10000,
    "d_c":      d_c,
    "l0":       l0,
    "d_l":      d_l,
    "d_p":      d_p,
    "T":        temp,
    "mu0":      mu0,
    "rho0":     rho0,
    "P0":       10,
    "P1":       0,
    "lx":       14,
    "ly":       20,
    "dP_per_l": 2.5e5,
    "t_max":    1,
    "E":        1e7,
    "d":        10e-9,
    "f":        f0,
    "MI":       MI,
}

np.random.seed(123)

p0 = 10
p1 = 0

sn = SimpleOpenTubeNetwork(nnodes=6)
sn.link_radius = 0.02
sn.setup()
s = Scale()

base_params["E"] *= 2e3

L = s.undim_length(base_params["l0"])
mu = s.undim_viscosity(base_params["mu0"])
E = s.undim_pressure(base_params["E"])
d = s.undim_length(base_params["d"])

# pressure at boundary in MPa
P = base_params["MI"] * np.sqrt(base_params["f"] / 1e6) * 1e6
P_undim = s.undim_pressure(P)

f = base_params["f"]
f_undim = s.dim_timestep(f)
T_undim = 1 / f_undim
T_tot = 5 * T_undim
dt = T_undim / 10000
t = np.arange(0, T_tot, dt)

p_bc = P_undim * np.sin(2 * np.pi * f_undim * t)
p_network = np.zeros(sn.nnodes)
# p_network[0] = p_bc[3]
# p = solve_elastic_node_pressures(sn,0,0,p_network, base_params, dt)

n_iters = 50000
first_link_radius = np.zeros(n_iters)
first_link_pressure = np.zeros(n_iters)
q_in = np.zeros(n_iters)
q_out = np.zeros(n_iters)
V_in = np.zeros(n_iters)
V_out = np.zeros(n_iters)
V_in_tmp = 0
V_out_tmp = 0
V_q = np.zeros(n_iters)
V_tot = np.zeros(n_iters)
V_ref = np.pi * sn.link_radius**2 * sn.link_length
V_nodes = np.zeros(n_iters)

p_bc[3 * n_iters // 5:] = 0

g_n = np.pi * sn.link_radii ** 4 / (8 * mu * L)
h_link = np.pi * sn.link_radii * sn.link_length * sn.link_radius ** 2 / (E * d)

for i in range(n_iters):
    # set boundary conditions for p_n
    p_network[0] = p_bc[i]
    p_network[-1] = 0

    # calculate p_n+1
    p_network_new = solve_elastic_node_pressures(sn, s, p_network, base_params, dt)

    dq_link = -h_link / dt * ((p_network_new[1:] - p_network[1:]) + (p_network_new[:-1] - p_network[:-1]))

    # the flow entering link at back
    q_back = -g_n * (p_network_new[1:] - p_network_new[:-1]) - dq_link / 2
    # the flow exiting link at front
    q_front = -g_n * (p_network_new[1:] - p_network_new[:-1]) + dq_link / 2

    #q_in[i] = q_back[0]
    V_in_tmp += q_back[0] * dt
    q_in[i] = q_back[0]
    V_in[i] = V_in_tmp

    q_out[i] = q_front[-1]
    V_out_tmp += q_front[-1] * dt
    V_out[i] = V_out_tmp

    V_nodes[i] = np.sum(q_back[1]-q_front[0])*dt

    V_q[i] = V_in[i] - V_out[i]

    p_ij = (p_network_new[:-1] + p_network_new[1:]) / 2

    # print(f"Link pressures: {p_ij}")
    # print(f"Link radii: {sn.link_radii}")
    first_link_radius[i] = sn.link_radii[0]
    first_link_pressure[i] = p_ij[0]
    p_network = p_network_new.copy()
    g_n = np.pi * sn.link_radii ** 4 / (8 * mu * L)
    sn.link_radii = sn.link_radius + sn.link_radius ** 2 / (E * d) * p_ij
    h_link = np.pi * sn.link_radii * sn.link_length * sn.link_radius ** 2 / (E * d)
    V_tot[i] = np.sum(np.pi * sn.link_radii**2*sn.link_length - V_ref)

plt.figure(dpi=400, figsize=plt.figaspect(0.5))

plt.subplot(231)
plt.plot(t[:n_iters], p_bc[:n_iters])
plt.grid(True)
plt.title("Pressure over time")

plt.subplot(232)
plt.plot(t[:n_iters], q_in)
plt.plot(t[:n_iters], q_out)
plt.legend(['q_in', 'q_out'], loc="upper right")
plt.grid(True)
plt.title("Flow over time")

plt.subplot(233)
plt.plot(t[:n_iters], V_in)
plt.plot(t[:n_iters], V_out)
plt.legend(['V_in', 'V_out'])
plt.grid(True)
plt.title("Volume over time")

plt.subplot(234)
plt.plot(t[:n_iters], V_q)
plt.plot(t[:n_iters], V_tot)
plt.legend(['V_q', 'V_tot'])
plt.grid(True)
plt.title("Total volume over time")

plt.subplot(235)
plt.plot(t[:n_iters], V_nodes)
plt.grid(True)
plt.title("Node volumes")

plt.show()
