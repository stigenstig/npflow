from npflow.constants import l0, mu0, rho0, r_l_ratio


class Scale(object):
    def __init__(self, l0=l0, mu0=mu0, rho0=rho0):
        """
        Class that uses the characteristic length, viscosity and density to make other
        variables dimensionless or dimensional.

        :param l0: characteristic length
        :param mu0: characteristic viscosity
        :param rho0: characteristic density
        """
        self.l0 = l0
        self.l0_inv = 1.0/self.l0

        self.mu0 = mu0
        self.mu0_inv = 1.0/self.mu0

        self.rho0 = rho0
        self.rho0_inv = 1.0/self.rho0

        self.dp0 = self.mu0**2/(self.rho0*self.l0**2)
        self.r0 = r_l_ratio*l0
        self.q0 = self.mu0*self.l0/self.rho0

        self.v0 = self.mu0*self.rho0_inv*self.l0_inv
        self.v0_inv = 1.0/self.v0

        self.dt0 = self.l0**2*self.rho0*self.mu0_inv
        self.dt0_inv = 1.0/self.dt0

    def dim_length(self, l_in):
        """
        :param l_in: dimensionless length
        :return: dimensional length
        """
        l_out = l_in*self.l0
        return l_out

    def undim_length(self, l_in):
        """
        :param l_in: dimensional length
        :return: dimensionless length
        """
        l_out = l_in*self.l0_inv
        return l_out

    def dim_viscosity(self, mu_in):
        """
        :param mu_in: dimensionless viscosity
        :return: dimensional viscosity
        """
        mu_out = mu_in*self.mu0
        return mu_out

    def undim_viscosity(self, mu_in):
        """
        :param mu_in: dimensional viscosity
        :return: dimensionless viscosity
        """
        mu_out = mu_in*self.mu0_inv
        return mu_out

    def dim_density(self, rho_in):
        """
        :param rho_in: dimensionless density
        :return: dimensional density
        """
        rho_out = rho_in*self.rho0
        return rho_out

    def undim_density(self, rho_in):
        """
        :param rho_in: dimensional density
        :return: dimensionless viscosity
        """
        rho_out = rho_in*self.rho0_inv
        return rho_out

    def dim_velocity(self, v_in):
        """
        :param v_in: dimensionless velocity
        :return: dimensional velocity
        """
        v_out = v_in*self.v0
        return v_out

    def undim_velocity(self, v_in):
        """
        :param v_in: dimensional velocity
        :return: dimensionless velocity
        """
        v_out = v_in*self.v0_inv
        return v_out

    def dim_timestep(self, dt_in):
        """
        :param dt_in: dimensionless timestep
        :return: dimensional timestep
        """
        dt_out = dt_in*self.dt0
        return dt_out

    def undim_timestep(self, dt_in):
        """
        :param dt_in: dimensional timestep
        :return: dimensionless timestep
        """
        dt_out = dt_in*self.dt0_inv
        return dt_out

    def dim_pressure(self, dp_in):
        dp_out = dp_in*self.dp0
        return dp_out

    def undim_pressure(self, dp_in):
        dp_out = dp_in/self.dp0
        return dp_out

    def dim_flow_rate(self, q_in):
        q_out = q_in*self.q0
        return q_out

    def undim_flow_rate(self, q_in):
        q_out = q_in / self.q0
        return q_out



