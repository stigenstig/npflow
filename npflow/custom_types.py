from enum import Enum, auto


class NodeTypes(Enum):
    NODE_NORMAL = auto()    # 1
    NODE_INLET = auto()     # 2
    NODE_OUTLET = auto()    # 3


class LinkTypes(Enum):
    LINK_NORMAL = auto()
    LINK_INLET = auto()
    LINK_OUTLET = auto()
    LINK_WRAP_Y = auto()
