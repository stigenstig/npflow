from setuptools import setup, find_packages

with open ('README.md') as f:
    readme = f.read()


required_packages = [
    'numpy', 'matplotlib', 'scipy', 'pytest', 'joblib'
]

required_python_version='>=3.6.0'

setup(
    name='npflow',
    description='Flow of nanoparticles',
    long_description=readme,
    author='Stig-Martin Liavåg',
    packages=find_packages(exclude=('tests',)),
    python_requires=required_python_version,
    install_requires=required_packages,
)
